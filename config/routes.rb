Rails.application.routes.draw do

  get 'menu_bar/search'

  get 'product_steps/show'

  get 'product_steps/update'

  get '/users/conversations/:id' => 'conversations#show'

  get 'video_chat/index'

  get 'video_chat/show'

  get 'menu_bar/show_users'

  get 'menu_bar/show_json'

  get 'video_chat/audio_call'

  post "/video_chat/index" => 'video_chat#index'

  get "/static_pages/update_users" => 'static_pages#update_users'

  get 'product_image/index'

  get 'product_image/new'

  get 'product_image/create'

  get 'product_image/edit'

  get 'product_image/update'

  get 'product_image/delete'

  get 'product_image/destroy'

  get 'sales/index'

  post 'sales/filter'

  get 'sales/filter'

  get 'sales/show'

  get 'sales/update'

  get 'sales/delete'

  get 'sales/destroy'

  get 'comments_product/index'

  get 'comments_product/new'

  get 'comments_product/create'

  get 'comments_product/update'

  get 'comments_product/delete'

  get 'comments_product/destroy'

  get 'order_items/create'

  get 'order_items/update'

  get 'order_items/destroy'

  get 'carts/show'

  get 'carts/delete'

  get 'product_carts/index'

  get 'product_carts/index_backend'

  get 'product_carts/new'

  get 'product_carts/create'

  get 'product_carts/show'

  get 'product_carts/edit'

  get 'product_carts/update'

  get 'product_carts/delete'

  get 'product_carts/destroy'

  get 'product_carts/show_modal'

  post 'product_carts/filter'

  get 'product_carts/show_video'

  get 'carts/index'

  get 'carts/index_backend'

  get 'carts/show_modal'

  post 'carts/filter'

  get 'products/new'

  get 'products/edit'

  get 'products/update'

  get 'products/create'

  get 'products/destroy'

  get 'products/list_products'

  get 'products/show'

  root 'static_pages#home'
  #root 'sessions#new'


  get 'help' => 'static_pages#help';

  get 'about' => 'static_pages#about';

  get 'contact' => 'static_pages#contact';

  get 'signup' => 'users#new'

  post 'signup' => 'users#new'

  get 'login' => 'sessions#new'

	post 'login' => 'sessions#create'

	delete 'logout' => 'sessions#destroy'

  delete 'carts/delete' => 'carts#destroy'

  post 'carts_steps/address' => 'carts_steps#create';

  post "/carts/address" => 'carts#create';

  post "/carts/delivery" => 'carts#update';

  post "/carts/complete" => 'carts#update';

  post 'carts/payment' =>'carts#payment';

  post 'carts_steps/payment' =>'carts_steps#payment';

  get '/users/subregion_options' => 'users#subregion_options'

  get '/users/subcategories_options' => 'users#subcategories_options'

  get '/users/audio_call'

  #get '/orders/subregion_options' => 'orders#subregion_options'

  resources :users do
    member do
      get :following, :followers
    end
  end

  resources :posts,           only: [:create, :destroy] do
    resources :comments,      only: [:create, :destroy]
  end

  resources :users

  resources :products

  #resources :posts,           only: [:create, :destroy] 
  #resources :comments,        only: [:create, :destroy]

  resources :relationships,   only: [:create, :destroy]

  resources :carts_steps

  resources :product_carts
  resources :comment_products
  resources :carts do
    get "delete"
  end
  resources :order_items, only: [:create, :update, :destroy]

  get 'videochat' => 'videochats#new'	

  get 'call' => 'videochats#index'

  resources :conversations do
    resources :messages
  end

  resource :profile_pictures

  resource :portraits


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
