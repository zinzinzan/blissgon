module StaticPagesHelper



  def conversation_interlocutor(conversation)
    conversation.recipient == current_user ? conversation.sender : conversation.recipient
  end


  def get_li_colors
    colors = %w(orange blue green purple gold)
    if @index_color == (colors.size-1) then
      @index_color = 0
      return colors.at(@index_color)
    else
      @index_color = @index_color + 1;
      return colors.at(@index_color)
    end
  end

end
