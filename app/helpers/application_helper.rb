module ApplicationHelper

  #------------------------------------------------------------

  def full_title(page_title = '')
    base_title = "Sttattus"
    if page_title.empty?
      base_title
    else
      " #{base_title} |  #{page_title} "
    end
  end

  #------------------------------------------------------------

  def javascript(*files)
    content_for(:head) { javascript_include_tag(*files) }
  end

  #------------------------------------------------------------

  def stylesheet(*files)
    content_for(:head) { stylesheet_link_tag(*files) }
  end

  #------------------------------------------------------------

  def glyph(*names)
    content_tag :i, nil, :class => names.map{|name| "glyphicon glyphicon-#{name.to_s.gsub('_','-')}" }
  end

  #------------------------------------------------------------

  def broadcast(channel, &block)
    message = {:channel => channel, :data => capture(&block)}
    uri = URI.parse("http://localhost:9292/faye")
    Net::HTTP.post_form(uri, :message => message.to_json)
  end

end
