module UsersHelper

  def gravatar_for(user, options = {size: 80})
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name)
  end

  def gravatar_for_chat(user, options = {size: 80})
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "chat-user-img")
  end

  def current_profile_pic
    @user.profile_pictures.each do |pics|
      if pics.current
        return pics
      end

    end
  end

  def current_portrait
    @user.portraits.each do |pics|
      if pics.current
        return pics
      end
    end
  end

  def current_profile_avatar
    current_user.profile_pictures.each do |pics|
      if pics.current
        return pics
      end
    end
  end

  def profile_avatar_user(user)
    user.profile_pictures.each do |pics|
      if pics.current
        return pics
      end
    end
  end

  def change_profile_pic(string)
#ajuro dos pasadas y que puede que primero haga true a pics.id.to_s == string
#y elk current quede current :S
    current_user.profile_pictures.each do |pics|
      if pics.current && pics.id.to_s != string
        pics.update_attribute(:current, false)
      elsif pics.current && pics.id.to_s == string
        return;
      end
    end

    current_user.profile_pictures.each do |pics|
      if pics.id.to_s == string
        pics.update_attribute(:current, true)
        flash[:success] = "Profile photo updated!"
      end
    end
  end

  def change_portrait_pic(string)
#ajuro dos pasadas y que puede que primero haga true a pics.id.to_s == string
#y elk current quede current :S
    current_user.portraits.each do |pics|
      if pics.current && pics.id.to_s != string
        pics.update_attribute(:current, false)
      elsif pics.current && pics.id.to_s == string
        return;
      end
    end

    current_user.portraits.each do |pics|
      if pics.id.to_s == string
        pics.update_attribute(:current, true)
        flash[:success] = "Portrait photo updated!"
      end
    end
  end


  def from_post_change_profile_pic(post_id)

    current_user.profile_pictures.each do |pics|
      pics.update_attribute(:current, false)
    end

    post = Post.find(post_id)
    prof_pic = ProfilePicture.new(picture: post.picture, current: true)
    if prof_pic.save
      current_user.profile_pictures.push(prof_pic)
      flash[:success] = "Profile photo updated!"
    else
      flash[:success] = "Error uploading the photo"
    end
  end


  def from_post_change_portrait_pic(post_id)

    current_user.portraits.each do |pics|
      pics.update_attribute(:current, false)
    end

    post = Post.find(post_id)
    prof_pic = Portrait.new(picture: post.picture, current: true)
    if prof_pic.save
      current_user.portraits.push(prof_pic)
      flash[:success] = "Cover photo updated!"
    else
      flash[:success] = "Error uploading the photo"
    end
  end




end