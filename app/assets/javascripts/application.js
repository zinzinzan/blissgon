// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require easyrtc.js
//= require jquery.steps.js
//= require audio_chat.js.erb
//= require video_chat_rtc.js.erb
//= require new_product_form.js.erb

//= require bootstrap
//= require jquery.bxslider
//= require jquery.fitvids
//= require bootstrap.min
//= require private_pub
//= require chat
//= require turbolinks
//= require jquery.min.js
//= require peer.js

//= require_tree .

