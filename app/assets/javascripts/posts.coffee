# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  $('select#user_country').change (event) ->
    select_wrapper = $('#order_state_code_wrapper')
    $('select', select_wrapper).attr('disabled', true)
    country_code = $(this).val()
    url = "/users/subregion_options?parent_region=#{country_code}"
    select_wrapper.load(url)

    $('#my_uploader').change (event) ->
      if($(this).val())
        #$('#my_submit_button').attr('disabled', '')
        $('#hidden_label').addClass('btn-primary')
      else

        # $('#my_submit_button').attr('disabled', 'false');

        #$('#hidden_label').addClass('btn-primary')

  $('select#user_category').change (event) ->
    select_wrapper2 = $('#sub_cate_select_wrapper')
    $('select', select_wrapper2).attr('disabled', true)
    category_id = $(this).val()
    url = "/users/subcategories_options?parent_category=#{category_id}"
    select_wrapper2.load(url)




  $(document).ready (event) ->
  $('[data-toggle="tooltip"]').tooltip()
