@import "bootstrap-sprockets";
@import "bootstrap";

/*vars*/
$bgDefault      : #F00;
$bgHighlight    : #c0392b;
$colDefault     : #ecf0f1;
$colHover       : #ffbbbc;

/*$liHover        : #: */
$liFont         : #000000;

/* mixins, variables, etc. */

$gray-medium-light: #eaeaea;

/* universal */

body {
background-image: url("/images/bg.jpg");
background-position-x: 50%;
background-position-y: 50%;
background-size: cover;
}

section {
  overflow: auto;
}

textarea {
  resize: vertical;
}

.center {
  text-align: center;
}

/* typography */

h2, h3, h4, h5, h6 {
  line-height: 1;
}

h1 {
  font-size: 1.8em;
  color: white;
  letter-spacing: -1px;
  font-weight: bold;
  &:hover {
    color: white;
    text-decoration: none;
  }
}

h2 {
  font-size: 1.4em;
  color: black !important;
  letter-spacing: -1px;
  font-weight: bold;
  &:hover {
    color: gray;
    text-decoration: none;
  }
}

h8 {
  font-size: 1.8em;
  color: white;
  letter-spacing: -1px;
  font-weight: bold;
  &:hover {
    color: white;
    text-decoration: none;
  }
}

p {
  font-size: 1.1em;
  line-height: 1.7em;
}


/* header */

#logo {
  float: left;
  margin-right: 10px;
  font-size: 1.7em;
  color: white;
  letter-spacing: -1px;
  padding-top: 9px;
  font-weight: bold;
  &:hover {
    color: white;
    text-decoration: none;
  }
}

.form-inline{
padding-top: 15px;
}

/* footer */

footer {
  margin-top: 45px;
  padding-top: 5px;
  border-top: 1px solid $gray-medium-light;
  color: $gray-light;
  a {
    color: $gray;
    &:hover {
      color: $gray-darker;
    }
  }
  small {
    float: left;
  }
  ul {
    float: right;
    list-style: none;
    li {
      float: left;
      margin-left: 15px;
    }
  }
}

@mixin box_sizing {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

/* Miseceellaneous */
.debug_dump{
  clear: both;
  float: left;
  width: 100;
  margin-top: 45px;
  @include box_sizing;
}


/* sidebar */

aside {
  section.user_info {
    margin-top: 20px;
  }
  section {
    padding: 10px 0;
    margin-top: 20px;
    &:first-child {
      border: 0;
      padding-top: 0;
    }
    span {
      display: block;
      margin-bottom: 3px;
      line-height: 1;
    }
    h1 {
      font-size: 1.4em;
      text-align: left;
      letter-spacing: -1px;
      margin-bottom: 3px;
      margin-top: 0px;
    }
  }
}

.gravatar {
  float: left;
  margin-right: 10px;
}

.gravatar_edit {
  margin-top: 15px;
}


/*forms*/

input, textarea, select, .uneditable-input{

  border: 1px solid #bbb;
  width: 100%;
  margin-bottom: 15px;
  margin: 1 !important;
  @include box_sizing;
}

input{

  height: auto !important;
}

#error_explanation {
  color: red;
  ul {
    color: red;
    margin: 0 0 30px 0;
  }
}

.field_with_errors {
  @extend .has-error;
  .form-control {
    color: $state-danger-text;
  }
}

.checkbox {
	margin-top: -10px;
	margin-bottom: 10px;
	span{
		margin-left: 20px;
		font-weigth: normal;
	}
}

#session_remember_me{
	width: auto;
	margin-left: 0;
}


/* --- Style --- */
.navbar-fixed-top {
    background-color: $bgDefault;
    border-color: $bgHighlight;
    .navbar-brand {
        color: $colDefault;
        &:hover, &:focus {
            color: $colHover; }}
    .navbar-nav {
        > li {
            > a {
                color: $colDefault;
                &:hover, &:focus {
                    color: $colHover;   }}}
        .active {
            > a, > a:hover, > a:focus {
                color: $colHover;
                background-color: $bgHighlight; }}
        .open {
            > a, > a:hover, > a:focus {
                color: $colHover;
                background-color: $bgHighlight;
                .caret {
                    border-top-color: $colHover;
                    border-bottom-color: $colHover; }}}
        > .dropdown {
            > a {
                .caret {
                    border-top-color: $colDefault;
                    border-bottom-color: $colDefault; }
                &:hover, &:focus {
                    .caret {
                        border-top-color: $colHover;
                        border-bottom-color: $colHover; }}}}}
    .navbar-toggle {
        border-color: $bgHighlight;
        &:hover, &:focus {
            background-color: $bgHighlight; }
        .icon-bar {
            background-color: $colDefault; }}}
@media (max-width: 767px) {
    .navbar-default .navbar-nav .open .dropdown-menu > li > a {
        color: $colDefault;
        &:hover, &:focus {
            color: $colHover;
            background-color: $bgHighlight; }}
}

.left {
    position: relative;
    right: 0px;
    width: 250px;	
}

.btn-sm{
min-width: 75px;
max-width: 75px;
}

.users {
	list-style: none;
	margin: 0;
	li{
		overflow: auto;
		padding: 10px 0;
		border-bottom: 1px solid $gray;
		a {
      color: #000 !important;
      font-weight: bold;
      font-size: 13px;
      font-family: 'Times New Roman', Times, serif;
}
	}

}

/*Posts*/

.posts {
  list-style: none;
  padding: 0;
  li{
    padding: 10px 0;
    border-top: 1px solid #e8e8e8;
  }
  .user {
    margin-top: 5em;
    padding-top: 0;
  }
  .content {
    display: block;
    margin-left: 60px;
    img {
      display: block;
      padding: 5px 0;
    }
  }
  .timestamp {
    color: $gray;
    display: block;
    margin-left: 60px;
  }
  .gravatar {
    float: left;
    margin: right;
    margin-top: 5px;
  }
}

aside {
  textarea {
    height: 100px;
    margin-bottom: 5px;
  }
 }
 
span.picture {
  margin-top: 10px;
  input{
    border: 0;
  }
} 

.navbar-default {
    background-color: $bgDefault;
    border-color: $bgHighlight;
}


.fitpadding {
   padding-left: 13px !important;
   padding-right: 13px !important;
}

.padding-top {
   padding-top: 15px !important;
}


.nopadding {
  padding-left: 0px !important;
  padding-right: 0px !important;
}

a {
 color: #FFFFFF !important;
 font-weight: bold;
 font-size: 13px;
 font-family: 'Times New Roman', Times, serif;
}

.navbar .nav > .dropdown.open.active > a:hover ,
.navbar .nav > .dropdown.open > a {
    background-color: #000 !important;
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#EEE), to(#BBB)) !important;
    background-repeat: repeat-x !important;
    color: #FFFFFF;
    outline: 0 none;
    text-decoration: none;
   
}

ul.dropdown-menu {
    background-color: #000 !important;
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#EEE), to(#BBB)) !important;
    background-repeat: repeat-x !important;
    color: #FFFFFF;
    outline: 0 none;
    text-decoration: none;   
}

.input-group {
    width:90% !important;
}

.sectionc2_group a {
height: 18px;
font-family: "Times New Roman", Times, serif;
font-size: 18px;
line-height: 26px;
color: #666 !important;
padding: 4px 8px;
}

            
.padding_top {
  padding-top: 120px;
}        

.padding_60top {
  padding-top: 60px;
}              

.padding_15top {
  padding-top: 15px;
}   

.padding_35top {
  padding-top: 30px;
}  

.sectionc2_group {
float: left;
width: 100%;
margin-top: 0px;
border: solid #CCC 1px;
background: #f1f3ff;
padding-top: 0px;
}

.section_a_1 {
width: 100%;
margin-top: 17px;
border: solid #FFF 8px;
box-shadow: 0px 1px 2px 1px #DBDBDB;
}

.head_section {
width: 100%;
height: 25px;
background: #FFF;
margin: 0;
padding: 0;
border: 0;
font-size: 100%;
font: inherit;
vertical-align: baseline;
}

.list_carousel {
margin: 10px auto 10px 3px;
width: 100%;
height: 255px;
}

.list_carousel {
	margin:10px auto 10px 3px;
	width: 100%;
	height:255px;
}

.list_carousel ul {
	margin: 0;
	padding: 0;
	list-style: none;
	display: block;
}

.list_carousel li {
	width: 145px;
	margin-right:10px;
	padding: 0;
	display: block;
	float: left;
}

.list_carousel li.last{ margin-right:0px; }

.sidebar {
    float: rigth;
}

.fixed {
   position: fixed;
   overflow-y: auto;
}

.stats {
  overflow: auto;
  margin-top: 0;
  padding: 0;
  a {
    float: left;
    padding: 0 10px;
    border-left: 1px solid $gray;
    color: gray !important;
    &:first-child {
      padding-left: 0;
      border: 0;
    }
    &:hover {
      text-decoration: none;
      color: blue;
    }
  }
  strong {
    display: block;
  }

} 

.user_avatars {
  overflow: auto;
  margin-top: 10px;
  .gravatar {
    margin: 1px 1px;
  }
  a {
    padding: 0;
  }
}

.users.follow {
  padding: 0;
}

