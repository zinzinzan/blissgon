class StaticPagesController < ApplicationController

  respond_to :html, :js

  before_action :set_cate
  before_filter :load_vars

  def home

    if logged_in?
      if (ConnectedUser.find_by(user_id: current_user.id)== nil)
        connected_user = ConnectedUser.new
        connected_user.user = current_user
        connected_user.save
      end

      @connected_users = ConnectedUser.where.not("user_id = ?", current_user.id).order("created_at DESC")
      @connected_users.each do |connected_user|
        connected_user.user.connected = true
        connected_user.user.save
      end
      connected_user_aux = ConnectedUser.find_by(user_id: current_user.id)
      connected_user_aux.user.connected = true
      connected_user_aux.user.save
      @users = User.where.not("id = ?", current_user.id).order("connected ASC")


      @conversations = Conversation.involving(current_user).order("created_at DESC")

      if params[:subcategory_id].present?
        @products_car = ProductCart.where(subcategory_id: params[:subcategory_id])

        @subcategory = Subcategory.find(params[:subcategory_id])
      else
        @subcategory = current_user.subcategory
        @products_car = ProductCart.where(subcategory_id: current_user.subcategory.id)
      end
      
      @order_item = current_order.order_items.new
      @order_items = current_order.order_items

      subcat_id = params[:subcategory_id]

      @ssfeed = sub_sub_cat_feed subcat_id
      @feed = current_user.feed subcat_id
      @feed_items = @feed.paginate(page: params[:page])

      respond_to do |format|
        format.html {
          @post = current_user.posts.build if logged_in?
        }
        format.js
      end

    else
      #ojo solo para pasar el user name de login session#new a signup user#new
      redirect_to login_path;
    end
  end

  def help
    @user = User.new
  end

  def about
    @user = User.new
  end

  def update_users


    if params[:close_window].present?
      connected_user = ConnectedUser.find_by(user_id: params[:user_id])
      if connected_user !=nil
        connected_user.user.connected = false
        connected_user.user.save
        connected_user.destroy
      end
      return
    end

    if params[:user_id].present?
      @users = User.where.not("id = ?", params[:user_id]).order("connected ASC")
      @current_user = User.find(params[:user_id])
      # @connected_users = ConnectedUser.where.not("user_id = ?", params[:user_id]).order("created_at DESC")
    end
  end

  protected

  def load_vars
    @index_color = -1;
  end

  def sub_sub_cat_feed subcat_id
    if subcat_id.nil?
      current_user.subcategory.subsubcategories
    else
      Subcategory.find(subcat_id).subsubcategories
    end

  end

  private

  def user_params
    params.permit(:user_id, :close_window)
  end


end
