class PortraitsController < ApplicationController

  before_action :logged_in_user, only: [:create, :new]

  #before_action :correct_user, only: [:create]

  include UsersHelper

  def new
    @picture_portrait = Portrait.new
  end

  def create
    #valida q ete logeado para que otro usurio pno puede crear img en nombre de otro
    # ya q si esta logeado es current user :trollface:
    @picture_portrait = current_user.portraits.build(image_params)
    if @picture_portrait.save
      change_portrait_pic @picture_portrait.id.to_s
      flash[:success] = "Portrait photo updated!"
      redirect_to user_path current_user
    else
      flash[:error] = "Error uploading the photo"
      redirect_to user_path current_user
    end
  end

=begin
  def correct_user
    @picture = current_user.profile_pictures.find_by(id: params[:id])
    redirect_to root_url if @picture.nil?
  end
=end

  def image_params
    params.require(:portrait).permit(:picture, :user_id, :current)
  end

end
