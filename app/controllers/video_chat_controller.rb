class VideoChatController < ApplicationController

  respond_to :html, :js


  def index
    @users = User.where.not("id = ?",current_user.id).order("created_at DESC")
    @current_user = current_user
    @connected_users = ConnectedUser.where.not("user_id = ?",
                                               current_user.id).order("created_at DESC")
  end

def show
  @current_user = current_user
  render :layout => false

end

def audio_call
  @current_user = current_user
  render :layout => false

end



end
