class CommentsController < ApplicationController

  before_action :logged_in_user, { only: [:create, :destroy] }

  before_action :correct_user, only: :destroy


#------------------------------------------------------------

  def create
    Rails.logger.debug params.inspect
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(content: params[:comment][:content], user_id: current_user.id )
    if @comment.save
      flash[:success] = "Comment created!"
      #redirect_to @post
      redirect_to root_url
    else
      @feed_items = []
      redirect_to :controller => 'static_pages', :action => 'home'
    end
  end

#------------------------------------------------------------

  def destroy
    @comment.destroy
    flash[:success] = "comment deleted"
#  is just the previous URL (in this case, the Home page)
    redirect_to request.referrer || root_url
  end

#------------------------------------------------------------

  private

  def correct_user
    if current_user.admin?
      post = Post.find(params[:post_id])
      @comment = post.comments.find_by(id: params[:id])
    else
      @comment = current_user.comments.find_by(id: params[:id])
      redirect_to root_url if @comment.nil?
    end
  end


end
