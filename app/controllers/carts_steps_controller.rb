class CartsStepsController < ApplicationController

  include Wicked::Wizard

  steps :address,:delivery,:payment,:complete

  def show
    @order = current_order
     if !current_order.address.nil?
      @address = current_order.address

      render_wizard
      return
    end
  else
    @address = Address.new
    render_wizard
  end


  def update
    @order = current_order
    if params[:id] =='address'
      @order = current_order
      @address = current_order.address
      @address.attributes = address_params
      @address.save
      render_wizard @order
      return
    end
    @order = current_order
    @order.attributes = order_params

    if params[:id] == 'complete'
      @sale = Sale.new
      for order_item in @order.order_items do
        @sale_item = SaleItem.new
        @sale_item.product_cart = order_item.product_cart
        @sale_item.total_price = order_item.total_price
        @sale_item.quantity = order_item.quantity
        @sale_item.unit_price = order_item.unit_price
        @sale_item.sale = @sale
        @sale_item.save
      end
      @sale.user = current_user
      @sale.subtotal = @order.subtotal
      @sale.total = @order.total
      @sale.save
      @order.order_status_id =3
      session[:order_id] =nil
    end


    if params[:id] == 'delivery'
      @order.total = @order.subtotal+@order.shipping
    end
    render_wizard @order
  end

  def create

    if !current_order.address.nil?
      redirect_to next_wizard_path
      return
    end
    @order = current_order
    @address = Address.new
    @address.attributes = address_params
    @order.address = @address
    @order.save
    redirect_to next_wizard_path
  end

  def payment
    transaction = AuthorizeNet::AIM::Transaction.new('2ysT948D2RSC', '6j569CB3jtJ36aw9',
                                                     :gateway=>:sandbox)
    credit_card = AuthorizeNet::CreditCard.new(params[:x_card_num], params[:x_card_code])
    @order = current_order
    response = transaction.purchase(@order.total, credit_card)

    if response.success?
      puts "Successfully made a purchase (authorization code: #{response.authorization_code})"
      redirect_to next_wizard_path
      return
    else
      #raise "Failed to make purchase."
      redirect_to next_wizard_path
      return
    end
  end

  private

  def order_params
    params.require(:order).permit(:shipping,:delivery_company,:payment)
  end



  def address_params
    params.require(:address).permit(:first_name,:last_name,:street_address,:zip_code,:phone)
  end

  def payment_params
    params.permit(:x_card_num,:x_exp_date,:x_card_code,:x_first_name,:x_last_name)
  end


end
