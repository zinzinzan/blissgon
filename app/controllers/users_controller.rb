class UsersController < ApplicationController

  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy, :following, :followers]

  before_action :correct_user, only: [:edit, :update]

  before_action :admin_user, only: [:destroy]

  before_action :user_posted_pics, only: [:show]

  before_filter :load_vars

  include UsersHelper

  def subregion_options
    render partial: 'subregion_select'
  end

  def subcategories_options
    render partial: 'subcategories_select'
  end

#------------------------------------------------------------------
  def show
    @user = User.find(params[:id])
    @posts = @user.posts.paginate(page: params[:page])
    @current_user = current_user

    @users = User.where.not("id = ?", current_user.id).order("created_at DESC")
    @conversations = Conversation.involving(current_user).order("created_at DESC")
    @products_car = ProductCart.where(user_id: current_user.id)
    @subcategory = current_user.subcategory


    @order_item = current_order.order_items.new
    @order_items = current_order.order_items

    subcat_id = params[:subcategory_id]

    @ssfeed = sub_sub_cat_feed subcat_id
    @feed = current_user.own_pots
    @feed_items = @feed.paginate(page: params[:page])
    @product = ProductCart.new
    @images = []
    for i in 0..3
      @image = ProductImage.new
      @images.append(@image)
    end

    @picture = ProfilePicture.new

    @picture_portrait = Portrait.new

    if params[:profile_picture_id]
      prof_pic_id = params[:profile_picture_id]
      change_profile_pic prof_pic_id
    end

    if params[:post_id]
      post_id = params[:post_id]
      if not params[:cover]
        from_post_change_profile_pic post_id

      else
        from_post_change_portrait_pic post_id
        # todas las del pref de usuario en false
        # agregar esta en el array de fotos de usuario y hacerla current true

      end
    end

    respond_to do |format|
      format.html {

      }
      format.js
    end

  end

#------------------------------------------------------------------

  def new
    @user = User.new
    @user.name = params[:name]
  end

#------------------------------------------------------------------

  def create
    @user= User.new(user_params)
    if @user.save
      log_in(@user);
      flash[:success] = "Welcome to Blissgon"
      redirect_to root_path
    else
      render 'new';
    end

#------------------------------------------------------------------

  end

  def edit
    @user = User.find(params[:id])

  end

#------------------------------------------------------------------

  def update

    @user = User.find(params[:id])

    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

#------------------------------------------------------------------

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url;
  end

#------------------------------------------------------------------

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

#------------------------------------------------------------------

  def index

    @users = User.paginate(page: params[:page])
  end

#------------------------------------------------------------------

  def admin_user
    redirect_to root_url unless current_user.admin?
  end

#------------------------------------------------------------------

  def following
    @title = "Following"
    @user = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

#------------------------------------------------------------------

  def followers
    @title = "Followers"
    @user = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

#------------------------------------------------------------------


  def audio_call

  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation,
                                 :subcategory_id, :birthdate,
                                 :country, :state, :phone)
  end

  protected

  def load_vars
    @index_color = -1;
  end

  def sub_sub_cat_feed subcat_id
    if subcat_id.nil?
      current_user.subcategory.subsubcategories
    else
      Subcategory.find(subcat_id).subsubcategories
    end
  end

  def user_posted_pics
    @posted_with_pics = []
    current_user.posts.each { |post|

      if post.picture?
        @posted_with_pics.push(post)
      end

    }

  end

end
