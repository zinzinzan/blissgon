class CommentProductsController < ApplicationController
  respond_to :html, :js

  def index
    @product_cart = ProductCart.find params[:comment_product][:id]
    @comments_product = @product_cart.comment_products
  end

  def new
    @comment_product = CommentProduct.new
    @product_cart = ProductCart.find params[:id]
  end

  def create
    @product_cart = ProductCart.find params[:comment_product][:id]
    @comments_product = @product_cart.comment_products
    @comment_product = @product_cart.comment_products.create(title: comment_product_params[:title],comment: comment_product_params[:comment], product_cart_id: @product_cart.id )
    @comment_product.user = current_user
    @comment_product.save

  end

  def update
    @comment_product = CommentProduct.find params[:id]
    @comment_product.update_attributes(comment_product_params)
  end

  def delete
    @comment_product = CommentProduct.find params[:comment_product][:id]
  end

  def destroy
    @comments_product = CommentProduct.all
    @comment_product = CommentProduct.find params[:comment_product][:id]
    @comment_product.destroy
  end


  private

  def comment_product_params
    params.require(:comment_product).permit(:id,:title,:comment)
  end

end
