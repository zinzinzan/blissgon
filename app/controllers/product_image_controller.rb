class ProductImageController < ApplicationController
  def index
    @product = ProductCart.find params[:product_cart_id]
    @images = @product.product_images
  end

  def new
    @image = ProductImage.new
  end

  def create
    @image = ProductImage.new(product_image_params)
    if @image.save
      redirect_to :action => 'index'
    end
  else
    redirect_to :action => 'new'
  end

  def edit
    @image = ProductImage.find params[:id]
  end

  def update
    @image = ProductImage.find params[:id]
    @image.update_attributes(product_image_params)
    redirect_to :action => 'index'
  end

  def delete
    @image = ProductImage.find params[:id]
  end

  def destroy
    #to do find the product's image before
    @product = ProductCart.find params[:product_cart_id]
    @images = @product.product_images
    @image = ProductImage.find params[:id]
    @image.destroy

  end

  private

  def product_image_params
    params.require(:product_image).permit(:id,:path,:product_cart_id)
  end


end
