class ProductsController < ApplicationController

  def list_products
    printf("list_products")
    @products = Product.all
  end

  def new
    @product = Product.new
  end

  def show
    @product = Product.find params[:id]
  end

  def edit
    @product = Product.find params[:id]
  end

  def update
    @product = Product.find params[:id]
    if @product.update_attributes(product_params)
      redirect_to :controller => 'static_pages', :action => 'home', :id => @product.id
    end
  end

  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to :action => 'show', :id => @product.id
    else
      render :action => 'new'
    end
  end

  def destroy
    printf("entre en destroy")
    @product= Product.find(params[:id]).destroy
    flash[:success] = "Product deleted"
    redirect_to :controller => 'static_pages', :action => 'home'
  end

  private

  def product_params
    params.require(:product).permit(:id,:name, :code,:stock,:price)
  end


end
