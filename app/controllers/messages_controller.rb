class MessagesController < ApplicationController

  respond_to :js

  def create
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.build(message_params)
    @message.user_id = params[:user_id]
    @message.save!

    @path = conversation_path(@conversation)
    render :format => :js
  end

  private

  def message_params
    params.permit(:body, :user_id,:conversation_id)
  end
end
