class PostsController < ApplicationController

before_action :logged_in_user, { only: [:create, :destroy] }

before_action :correct_user, only: :destroy

#------------------------------------------------------------

def create
  @post = current_user.posts.build(post_params)
  if @post.save

    flash[:success] = "Post created!"
=begin
    if params[:video].present?
      @post.video = params[:video]
      @post.save
    end
=end
    redirect_to root_url
  else
    @feed_items = []
    redirect_to root_url
  end
end

#------------------------------------------------------------

def destroy
  @post.destroy
  flash[:success] = "Post deleted"
#  is just the previous URL (in this case, the Home page)
  redirect_to request.referrer || root_url 
end

#------------------------------------------------------------

private

  def post_params
    params.require(:post).permit(:content, :picture, :video	)
  end


=begin
  def extra_params
    params.permit(:video)

  end
=end

#------------------------------------------------------------

  def correct_user
    if current_user.admin?
      @post = Post.find(params[:id])
    else
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end

  end

end
