class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  include SessionsHelper
  helper_method :current_order

  # http_basic_authenticate_with name: "bliss",  :password => "#=]c-pvX%-Dk"

  before_action :set_cate
  #------------------------------------------------------------------

  def set_cate
    @enter = Category.find_by(name: "Entertainment")
    @enter_subs = @enter.subcategories.all

    @indus = Category.find_by(name: "Industry")
    @indus_subs = @indus.subcategories.all

    @edu = Category.find_by(name: "Education")
    @edu_subs = @edu.subcategories.all

    @found = Category.find_by(name: "Foundations")
    @found_subs = @found.subcategories.all

    @beau = Category.find_by(name: "Health/Beauty")
    @beau_subs = @beau.subcategories.all
  end

	def logged_in_user

		unless logged_in?
			flash[:danger] = "Please log in"
			redirect_to login_url
		end
  end

  def current_order
    order = Order.where("orders.user_id = :user_id AND orders.order_status_id != :order_shipped AND orders.order_status_id != :order_placed ",
    {user_id: current_user.id , order_shipped: '3',order_placed: '2'})

    if order[0] !=nil
      return order[0]
    end

   return Order.new

  end


  def get_users
    @users = User.all
  end



end
