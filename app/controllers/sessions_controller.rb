class SessionsController < ApplicationController

  def new
    #ojo solo para pasar el user name de login session#new a signup user#new
    @user = User.new
  	if logged_in? 
  	  #if log in
			redirect_to root_url
  	end
  end

  def create

    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_to root_url
    else
      flash.now[:danger] = "Invalid email/password combination"
      #ojo solo para pasar el user name de login session#new a signup user#new
      @user = User.new
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url;
  end

end