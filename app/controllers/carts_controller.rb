class CartsController < ApplicationController

  respond_to :html, :js

  include Wicked::Wizard

  steps :address,:delivery,:payment,:complete

  def index
    @order_items = current_order.order_items
    @order = current_order
    @order_checkout = current_order
    if !current_order.address.nil?
      @address = current_order.address
    end
  else
    @address = Address.new

  end

  def index_backend

    if current_user.admin == true
      @orders = Order.all.paginate(page: params[:page], per_page: 1)
      @products_cart = ProductCart.all
      return
    end
  if current_user.admin != true
    @orders = Order.where("orders.user_id = :user_id",{user_id: current_user.id})
    @orders = @orders.paginate(page: params[:page], per_page: 2)
    return
  end
  end

  def filter
    @orders = Order.where(nil)
    if params[:initial_date].present? && params[:final_date].present?
      @orders = Order.joins(:user)
      @orders = @orders.where("orders.created_at >= :initial_date AND orders.created_at <= :final_date",
                              {initial_date: params[:initial_date], final_date: params[:final_date]})
    end

    if params[:order_status].present?
      if params[:order_status].present?
        @orders = Order.joins(:order_status)
        @orders = @orders.where("order_statuses.id = :order_status_id",
                                {order_status_id: params[:order_status]})
      end
    end



    if params[:starts_with].present?
      @orders = Order.joins(:user)
      @orders = @orders.starts_with(params[:starts_with])
    end

  end



  def edit
    @order_item = current_order.order_items.find params[:id]

  end

  def delete
    @order_item = current_order.order_items.find params[:id]
  end

  def destroy
    @order_items = current_order.order_items
    @order_item = current_order.order_items.find params[:id]
    @order_item.destroy
    redirect_to :controller => 'carts',:action => 'index'
  end

  def show
    @order_checkout = current_order
    if !current_order.address.nil?
      @address = current_order.address
      render_wizard
      return
    end
    @address = Address.new
    render_wizard
  end

  def update
    @order_checkout = current_order
    if params[:id] =='address'
      @address = current_order.address
      @address.attributes = address_params
      @address.save
      render_wizard @order_checkout
      return
    end

    if params[:id] == 'complete'
      @sale = Sale.new
      for order_item in @order_checkout.order_items do
        @sale_item = SaleItem.new
        @sale_item.product_cart = order_item.product_cart
        @sale_item.total_price = order_item.total_price
        @sale_item.quantity = order_item.quantity
        @sale_item.unit_price = order_item.unit_price
        @sale_item.sale = @sale
        @sale_item.save
      end
      printf("PUTAAAAAA #{@order_checkout.total}")
      printf("PUTAAAAAA #{@order_checkout.shipping}")
      @sale.user = current_user
      @sale.subtotal = @order_checkout.subtotal
      @sale.total = @order_checkout.total
      @sale.save
      @order_checkout.order_status_id =3
      @order_checkout.save
      # session[:order_id] =nil
      redirect_to finish_wizard_path
      return
    end


    if params[:id] == 'delivery'
      @order_checkout.shipping = params[:shipping]
      @order_checkout.delivery_company = params[:delivery_company]
      @order_checkout.total = @order_checkout.subtotal+@order_checkout.shipping
      @order_checkout.save
      render_wizard @order_checkout
      return
    end
    render_wizard @order_checkout
  end

  def create

    if !current_order.address.nil?
      redirect_to next_wizard_path
      return
    end
    @order_checkout = current_order
    @address = Address.new
    @address.attributes = address_params
    @order_checkout.address = @address
    @order_checkout.save
    redirect_to next_wizard_path
  end


  def payment
    transaction = AuthorizeNet::AIM::Transaction.new('2ysT948D2RSC', '6j569CB3jtJ36aw9',
                                                     :gateway=>:sandbox)
    credit_card = AuthorizeNet::CreditCard.new(params[:x_card_num], params[:x_card_code])
    @order_checkout = current_order
    response = transaction.purchase(@order_checkout.total, credit_card)

    if response.success?
      puts "Successfully made a purchase (authorization code: #{response.authorization_code})"
      render_wizard @order_checkout
      return
    else
      #raise "Failed to make purchase."
      render_wizard @order_checkout
      return
    end
  end


  private
  def order_params
    params.permit(:id,:order,:product_carts,:quantity)
  end

  def filtering_params(params)
    params.slice(:starts_with,:initial_date,:final_date,:order_status)
  end

  def purchase_params
    params.permit(:shipping,:delivery_company,:payment)
  end

  def address_params
    params.permit(:first_name,:last_name,:street_address,:zip_code,:phone)
  end

  def payment_params
    params.permit(:x_card_num,:x_exp_date,:x_card_code,:x_first_name,:x_last_name)
  end

  def delivery_params
    params.permit(:delivery_company,:shipping)
  end


end
