class MenuBarController < ApplicationController
  def search
    printf("entre aqui")
  end


  def show_users
    @users = User.where("lower(name) like (?)", params[:user_to_search]+"%")
  end


  def show_json
    @users = User.where("lower(name) like (?)", params[:q]+"%")
  end


  private


  def menu_params
    params.permit(:q, :user_to_search)
  end
end
