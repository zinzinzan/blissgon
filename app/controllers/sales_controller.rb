class SalesController < ApplicationController

  respond_to :html, :js
  def index
    @sales = Sale.all
    @sale_list = []

    if current_user.admin == true
      @sale_list = Sale.all
      @sale_list = @sale_list.paginate(page: params[:page], per_page: 2)
      return
    end
    for sale in @sales do
      for sale_item in sale.sale_items do
        if sale_item.product_cart.user == current_user
          @sale_list.append(sale)
        end
      end
    end
    @sale_list = @sale_list.paginate(page: params[:page], per_page: 2)
  end

  def filter
    sale_list_aux =[]
    if params[:starts_with].present?
      @user = User.where("users.email like :email",{email: "#{params[:starts_with]}%"})
      if @user.count >= 2

        for user in @user do
          if user.sales !=nil
            for sale in user.sales
              sale_list_aux.append(sale)
            end
          end
        end
        @sale_list = sale_list_aux
        return
      end

      @sale_list = Sale.where("sales.user_id =:user_id",{user_id: @user[0].id})


    end
    if params[:initial_date].present? and params[:final_date].present?
      @sale_list = Sale.where("sales.created_at >= :initial_date and sales.created_at <= :final_date",
      {initial_date: params[:initial_date], final_date: params[:final_date]})
    end

  end

  def show
    @sale = Sale.find params[:id]
  end

  def update
  end

  def delete
  end

  def destroy
  end

  private

  def sale_params
    params.require(:sale).permit(:id,:user_id,:created_at,:start_with)
  end

end

