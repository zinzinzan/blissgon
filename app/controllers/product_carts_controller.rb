class ProductCartsController < ApplicationController

  respond_to :html, :js


  def index
    if params[:subcategory_id].present?
      @products_car = ProductCart.where(subcategory_id: params[:subcategory_id])
      @subcategory = Subcategory.find(params[:subcategory_id])
    else
      @products_car = ProductCart.where(subcategory_id: current_user.subcategory.id)
      @subcategory = current_user.subcategory
    end
    @order_item = current_order.order_items.new
  end


  def index_backend
    if current_user.admin
      @products = ProductCart.all
      @products = @products.paginate(page: params[:page], per_page: 2)
      return
    end

    @products = ProductCart.where("product_carts.user_id = :user_id",
                                  {user_id: current_user.id})
    if @products !=nil
      @products = @products.paginate(page: params[:page], per_page: 2)
    end
  end

  def new

    @product = ProductCart.new
    @images = []
    for i in 0..3
      @image = ProductImage.new
      @images.append(@image)
    end

  end

  def create
    @images = []
    for i in 0..3
      @image = ProductImage.new
      @images.append(@image)
    end
    @product = ProductCart.new(product_params)
    @product.user = current_user
    @last_product = ProductCart.last
    @product.type_product = params[:type_product]

    if @last_product !=nil
      @product.id = @last_product.id+1
    end
    @product.subcategory = current_user.subcategory

    if params[:price].present?
      @product.price = params[:price]
    else
      @product.price =0
    end

    if params[:delivery].present?
      @product.delivery_price = params[:delivery]
    end

    if params[:rent_time].present?
      @product.rent_time = params[:rent_time]
    end
    @product.type_sale = params[:sale_type]

    if @product.save

      if params[:country].present?
        @product.origin_country = params[:country][:id]
        @product.save
      end

      if params[:domestic_delivery_field].present?
        @product.domestic_delivery = params[:domestic_delivery_field]
        @product.price_for_extra_unit_domestic= params[:domestic_extra_price_delivery]
        @product.save
      end
      if params[:international_delivery_field].present?
        @product.international_delivery = params[:international_delivery_field]
        @product.price_for_extra_unit_international = params[:international_extra_price_delivery]
        @product.save
      end

      if params[:video].present?
        @product.video = params[:video]
        @product.save
      end

      if params[:product_file]
        @product.file_product = params[:product_file]
        @product.save
      end

      if params[:picture_product_0].present?
        @image0 = @product.product_images.create(picture_product: params[:picture_product_0], product_cart_id: @product.id)
        @image0.save
      end

      if params[:picture_product_1].present?
        @image1 = @product.product_images.create(picture_product: params[:picture_product_1], product_cart_id: @product.id)
        @image1.save
      end

      if params[:picture_product_2].present?
        @image2 = @product.product_images.create(picture_product: params[:picture_product_2], product_cart_id: @product.id)
        @image2.save
      end

      if params[:picture_product_3].present?
        @image3 = @product.product_images.create(picture_product: params[:picture_product_3], product_cart_id: @product.id)
        @image3.save
      end
      redirect_to :action => 'index_backend'
    else
      render :action => 'new'
    end
  end

  def

  show
    @product_car = ProductCart.find (params[:id])
    @comments_product= @product_car.comment_products

    @comments_product = @comments_product.paginate(page: params[:page], per_page: 6)
    @order_item = current_order.order_items.new
    @comment_product = CommentProduct.new
    @current_user = current_user

    @related_products = ProductCart.where("subcategory_id = ? ", @product_car.subcategory.id).limit(3)
    @user_products = ProductCart.where("user_id = ? ", @product_car.user.id).limit(3)

  end

  def edit
    @product = ProductCart.find params[:id]
    @image_edit = @product.product_images
    printf("putaaaaaaaa    #{@product.file_product}")

    if @image_edit.length == 0
      @image_edit = []
      for i in 0..3
        @image = ProductImage.new
        @image_edit.append(@image)
      end
      return
    end

    if @image_edit.length > 0 and @image_edit.length <= 3
      n =0
      case @image_edit.length
        when 1
          n = 2
        when 2
          n =1
        when 3
          n=0
      end
      for i in 0..n
        @image = ProductImage.new
        @image_edit.append(@image)
      end
    end

  end

  def update
    @product = ProductCart.find params[:id]
    @product.update_attributes(product_params)

    if params[:video].present?

    end

    if params[:country].present?
      @product.origin_country = params[:country][:id]
      @product.save
    end

    if params[:product_file]
      @product.file_product = params[:product_file]
      @product.save
    end


    if params[:delivery].present?
      @product.delivery_price = params[:delivery]
      @product.save
    end

    if params[:rent_time].present?
      @product.rent_time = params[:rent_time]
      @product.save
    end

    if params[:country].present?
      @product.origin_country = params[:country][:id]
      @product.save
    end

    if params[:domestic_delivery_field].present?
      @product.domestic_delivery = params[:domestic_delivery_field]
      @product.price_for_extra_unit_domestic= params[:domestic_extra_price_delivery]
      @product.save
    end
    if params[:international_delivery_field].present?
      @product.international_delivery = params[:international_delivery_field]
      @product.price_for_extra_unit_international = params[:international_extra_price_delivery]
      @product.save
    end

    if params[:picture_product_0].present?
      @image0 = @product.product_images[0]
      if @image0 == nil
        @image0 = @product.product_images.create(picture_product: params[:picture_product_0], product_cart_id: @product.id)
        @image0.save
      else
        @image0.picture_product = params[:picture_product_0]
        @image0.save
      end
    end

    if params[:picture_product_1].present?
      @image1 = @product.product_images[1]
      @image1.picture_product = params[:picture_product_1]
      @image1.save
    end

    if params[:picture_product_2].present?
      @image2 = @product.product_images[2]
      @image2.picture_product = params[:picture_product_2]
      @image2.save

    end

    if params[:picture_product_3].present?
      @image3 = @product.product_images[3]
      @image3.picture_product = params[:picture_product_3]
      @image3.save

    end
    redirect_to :action => 'index_backend'
    return

  end

  def delete
    @product = ProductCart.find params[:id]
    @image = ProductImage.find params[:id_image]
  end

  def destroy
    if current_user.admin
      @products = ProductCart.all
      @products = @products.paginate(page: params[:page], per_page: 2)
      return
    end
    @products = ProductCart.where("product_carts.user_id = :user_id",
                                  {user_id: current_user.id})
    @product = ProductCart.find params[:id]
    @product.destroy

  end

  def filter
    puts "entre en filter"
    @products = ProductCart.where(nil)
    @products = @products.starts_with(params[:starts_with]) if params[:starts_with].present?
  end

  def show_video
    subcategory_id = params[:subcategory_id]

    if subcategory_id != current_user.subcategory.id
      @products_car = ProductCart.where(subcategory_id: params[:subcategory_id])
      @subcategory = Subcategory.find(params[:subcategory_id])
    else
      @products_car = ProductCart.where(subcategory_id: current_user.subcategory.id)
      @subcategory = current_user.subcategory
    end
  end

  private

  def filtering_params(params)
    params.slice(:active, :starts_with)
  end

  def product_params
    params.require(:product_cart).permit(:id, :name, :price, :active, :type_product, :sale_type,
                                         :rent_time, :domestic_delivery_field, :domestic_extra_price_delivery, :international_delivery_field, :international_extra_price_delivery,
                                         :picture_product_0, :picture_product_1, :picture_product_2, :picture_product_3, :video, :product_file)
  end

  def country_params
    params.require(:country).permit(:id)
  end


end
