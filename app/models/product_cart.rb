class ProductCart < ActiveRecord::Base
  has_many :order_items
  has_many :sale_items
  has_many :comment_products, dependent: :destroy
  has_many :product_images, dependent: :destroy
  belongs_to :user
  belongs_to :subcategory
  mount_uploader :video, VideoUploader
  mount_uploader :file_product, FileUploader

  def set_success(format, opts)
    self.success = true
  end

  # default_scope { where(active: true) }
  scope :starts_with, -> (name) { where("name like ?", "#{name}%")}
end
