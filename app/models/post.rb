class Post < ActiveRecord::Base

  belongs_to :user
  has_many :comments, dependent: :destroy
  default_scope -> { order(created_at: :desc) }

#------------------------------------------------------------
  mount_uploader :picture, PictureUploader
  mount_uploader :video, VideoUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 2024 } 
  validate :picture_size


#------------------------------------------------------------

  def feed_comments
    Comment.where("post_id = :post_id", post_id: id)  #no escala!!
  end


  def set_success(format, opts)
    self.success = true
  end

#------------------------------------------------------------

  private

    def picture_size
      if  ( picture.size > 5.megabytes )
        errors.add(:picture, "should be less than 5MB")
      end
    end



end