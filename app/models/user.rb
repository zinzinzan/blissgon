class User < ActiveRecord::Base
  has_many :posts, dependent: :destroy
  has_many :orders
  has_many :sales
  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy

  has_many :pasive_relationships, class_name:   "Relationship",
                                  foreign_key:  "followed_id",
                                  dependent:    :destroy
                                  
  has_many :followers, through: :pasive_relationships, source: :follower                                
  has_many :following, through: :active_relationships, source: :followed

  has_many :comments, through: :posts, dependent: :destroy

  has_many :product_carts
  has_many :comment_products

  belongs_to :subcategory
  has_one :subsubcategory, through: :subcategory

  has_one :category, through: :subcategory

  has_many :conversations, :foreign_key => :sender_id

  has_many :profile_pictures

  has_many :portraits

  attr_accessor :remember_token

  before_save {self.email = email.downcase}
  validates(:name, presence: true, length: {maximum: 50} )
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates(:email, presence: true, length: {maximum: 255}, format:{ with: VALID_EMAIL_REGEX }, uniqueness: {case_sensitive: false})
  has_secure_password
  validates(:password, length: {minimum: 6}, allow_blank: true)
  validates(:subcategory_id, presence: true)
  validates(:birthdate, presence: true)
  validates(:country, presence: true )
  validates(:state, presence: true)
  validates(:phone, presence: true)
  #------------------------------------------------------------
  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? 
                                         BCrypt::Engine::MIN_COST : 
                                         BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  #------------------------------------------------------------
  def User.new_token
    SecureRandom.urlsafe_base64;
  end
  #------------------------------------------------------------
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest,User.digest(self.remember_token)) 
  end

  #------------------------------------------------------------

=begin
  def update_id_call
    update_attribute(:id_for_call, User.new_token )
  end

  #------------------------------------------------------------

  def delete_id_call
    update_attribute(:id_for_call, nil ) 
  end
=end

  #------------------------------------------------------------

  def authenticated?(remember_token)
  	return false if remember_digest.nil?
  	BCrypt::Password.new(remember_digest).is_password?(remember_token)
	end
	
	#------------------------------------------------------------
	
	def forget
		update_attribute(:remember_digest, nil )
	end
	
  #------------------------------------------------------------

  def feed(sub_cat_id )
    sub_cat_id = self.subcategory_id unless
        sub_cat_id
=begin
    Rails.logger.debug("----------------------------------------")
    Rails.logger.debug("----------------------------------------")
    Rails.logger.debug("My object: #{integer.inspect}")
    Rails.logger.debug("----------------------------------------")
    Rails.logger.debug("----------------------------------------")
=end
    #Post.where("user_id IN (:following_ids) OR user_id = :user_id", following_ids: following_ids, user_id: id) // no escala!!
    #This code has a formidable combination of Rails, Ruby, and SQL, but it does the job, and does it well:

    #add index to category_id

    #The UNION operator selects only distinct values by default.

    #following_ids = "select followed_id from relationships where follower_id = :user_id

    all_users_ids_posts = "(select followed_id from relationships where follower_id = :user_id)
                    UNION
                    ( select id from users where subcategory_id = :subcategory_id)";

    Post.where("user_id IN (#{all_users_ids_posts})", {user_id: id, subcategory_id: sub_cat_id} )
    #Of course, even the subselect won’t scale forever.
    # For bigger sites, you would probably need to generate the feed asynchronously using a background job,
    # but such scaling subtleties are beyond the scope of this tutorial.
  end

  def own_pots
    Post.where("user_id = :user_id", {user_id: id });
  end

  #------------------------------------------------------------
  def follow (other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  #------------------------------------------------------------
  def unfollow (other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  #------------------------------------------------------------
  def following?(other_user)
    following.include?(other_user)
  end

end