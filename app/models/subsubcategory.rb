class Subsubcategory < ActiveRecord::Base
  belongs_to :subcategory
  has_one :category, through: :subcategory
  has_one :user, through: :subcategory
end
