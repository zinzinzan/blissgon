class Subcategory < ActiveRecord::Base
  belongs_to :category
  has_many :users
  has_many :product_carts
  has_many :subsubcategories, dependent: :destroy

  validates( :category_id, presence: true )
end
