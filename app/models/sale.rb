class Sale < ActiveRecord::Base
  has_many :sale_items
  before_save :update_subtotal
  belongs_to :user

  def subtotal
    sale_items.collect { |oi| oi.valid? ? (oi.quantity * oi.unit_price) : 0 }.sum
  end
  private

  def update_subtotal
    self[:subtotal] = subtotal
  end


end
