class SaleItem < ActiveRecord::Base
  belongs_to :product_cart
  belongs_to :sale

  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validate :product_present
  validate :sale_present

  def unit_price
    if persisted?
      self[:unit_price]
    else
      product_cart.price
    end
  end

  def total_price
    unit_price * quantity
  end

  private
  def product_present
    if product_cart.nil?
      errors.add(:product_carts, "is not valid or is not active.")
    end
  end

  def sale_present
    if sale.nil?
      errors.add(:sale, "is not a valid sale.")
    end
  end

  def finalize
    self[:unit_price] = unit_price
    self[:total_price] = quantity * self[:unit_price]
  end


end
