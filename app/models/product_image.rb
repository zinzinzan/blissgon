class ProductImage < ActiveRecord::Base
  belongs_to :product_cart
  mount_uploader :picture_product, PictureUploader
end
