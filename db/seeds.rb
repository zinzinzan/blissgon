# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

#creando las categorias

enter = Category.create!(name: "Entertainment")
indus = Category.create!(name: "Industry")
edu = Category.create!(name: "Education")
heal_beauty= Category.create!(name: "Health/Beauty")
fund = Category.create!(name: "Foundations")
#------------------------------------------------------------
# Entertaiment
#------------------------------------------------------------
sub_music = enter.subcategories.create!(name: "Music")
#------------------------------------------------------------
####Entertaiment Subs
#------------------------------------------------------------


#------------------------------------------------------------
# Sport-Category
#------------------------------------------------------------
sub_enter_sport = enter.subcategories.create!(name: "Sport")
sub_enter_sport.subsubcategories.create!(name: "MLB")
sub_enter_sport.subsubcategories.create!(name: "MLS")
sub_enter_sport.subsubcategories.create!(name: "Box")
sub_enter_sport.subsubcategories.create!(name: "UFC")
sub_enter_sport.subsubcategories.create!(name: "NHL")
sub_enter_sport.subsubcategories.create!(name: "NFL")
sub_enter_sport.subsubcategories.create!(name: "NBA")
sub_enter_sport.subsubcategories.create!(name: "Golf")
sub_enter_sport.subsubcategories.create!(name: "Chess")
sub_enter_sport.subsubcategories.create!(name: "tennis")
sub_enter_sport.subsubcategories.create!(name: "NASCAR")
sub_enter_sport.subsubcategories.create!(name: "IndyCar")
sub_enter_sport.subsubcategories.create!(name: "F1")
sub_enter_sport.subsubcategories.create!(name: "Wrestling")
sub_enter_sport.subsubcategories.create!(name: "Rugby")
sub_enter_sport.subsubcategories.create!(name: "hockey sobre césped")
sub_enter_sport.subsubcategories.create!(name: "esgrima__?")
sub_enter_sport.subsubcategories.create!(name: "lucha greco")
sub_enter_sport.subsubcategories.create!(name: "Wrestling")
sub_enter_sport.subsubcategories.create!(name: "biking__?")
sub_enter_sport.subsubcategories.create!(name: "Volleyball")
sub_enter_sport.subsubcategories.create!(name: "Beach volleyball")
sub_enter_sport.subsubcategories.create!(name: "Weightlifting")
sub_enter_sport.subsubcategories.create!(name: "Volleyball")
sub_enter_sport.subsubcategories.create!(name: "Table tennis")
sub_enter_sport.subsubcategories.create!(name: "Athletics")
sub_enter_sport.subsubcategories.create!(name: "Montain bike")
sub_enter_sport.subsubcategories.create!(name: "Volleyball")
sub_enter_sport.subsubcategories.create!(name: "Adventure sports")
sub_enter_sport.subsubcategories.create!(name: "Skiing")
sub_enter_sport.subsubcategories.create!(name: "Cricket")
sub_enter_sport.subsubcategories.create!(name: "Swimming")
sub_enter_sport.subsubcategories.create!(name: "Synchronized swimming")
sub_enter_sport.subsubcategories.create!(name: "paddle")
#--------------------Sub Music---------------------

sub_music.subsubcategories.create!(name: "Sound technicians")
sub_music.subsubcategories.create!(name: "Stores instruments")
sub_music.subsubcategories.create!(name: "Sound engineers")
sub_music.subsubcategories.create!(name: "Arreglistas__?")
sub_music.subsubcategories.create!(name: "Musical producers")
sub_music.subsubcategories.create!(name: "Academies")
sub_music.subsubcategories.create!(name: "Teachers")
sub_music.subsubcategories.create!(name: "Conservatories")
sub_music.subsubcategories.create!(name: "Record stores__?")
sub_music.subsubcategories.create!(name: "Dancer")
sub_music.subsubcategories.create!(name: "Composers")
sub_music.subsubcategories.create!(name: "Dressing")
sub_music.subsubcategories.create!(name: "Montadores de escenarios__?")
sub_music.subsubcategories.create!(name: "Drummers")
sub_music.subsubcategories.create!(name: "Pianists")
sub_music.subsubcategories.create!(name: "Saxophonists")
sub_music.subsubcategories.create!(name: "Guitarists")
sub_music.subsubcategories.create!(name: "Singer")
sub_music.subsubcategories.create!(name: "Violinist")
sub_music.subsubcategories.create!(name: "Trumpeter")
sub_music.subsubcategories.create!(name: "Bassist__?")
sub_music.subsubcategories.create!(name: "Vocalistas")

#--------------------Sub Movie--------------------
sub_movie = enter.subcategories.create!(name: "Movies")

sub_movie.subsubcategories.create!(name: "Production Companies")
sub_movie.subsubcategories.create!(name: "Film instruments")
sub_movie.subsubcategories.create!(name: "Casting agencies")
sub_movie.subsubcategories.create!(name: "Filmmakers")
sub_movie.subsubcategories.create!(name: "Actors")
sub_movie.subsubcategories.create!(name: "Writers")
sub_movie.subsubcategories.create!(name: "Cameras")
sub_movie.subsubcategories.create!(name: "Assistants")
sub_movie.subsubcategories.create!(name: "Double")
sub_movie.subsubcategories.create!(name: "Photographers")
sub_movie.subsubcategories.create!(name: "Stylists")
sub_movie.subsubcategories.create!(name: "Makeup")
sub_movie.subsubcategories.create!(name: "Transporters")
sub_movie.subsubcategories.create!(name: "Assemblers scenario")
sub_movie.subsubcategories.create!(name: "Distribution stores")
sub_movie.subsubcategories.create!(name: "Teachers")
#--------------------Sub Televisión--------------------
sub_tele = enter.subcategories.create!(name: "Televisión")

sub_tele.subsubcategories.create!(name: "Presenters")
sub_tele.subsubcategories.create!(name: "Series")
sub_tele.subsubcategories.create!(name: "Novels")
sub_tele.subsubcategories.create!(name: "Shows")
sub_tele.subsubcategories.create!(name: "Public channels")
sub_tele.subsubcategories.create!(name: "Pay per View")
#--------------------Sub Radio--------------------
sub_radio = enter.subcategories.create!(name: "Radio")

sub_radio.subsubcategories.create!(name: "Speakers")
sub_radio.subsubcategories.create!(name: "FM Radio")
sub_radio.subsubcategories.create!(name: "AM Radio")
sub_radio.subsubcategories.create!(name: "Novels")
#--------------------Sub Travel-------------------
sub_travel = enter.subcategories.create!(name: "Travel")

sub_travel.subsubcategories.create!(name: "Excursions")
sub_travel.subsubcategories.create!(name: "Hotels")
sub_travel.subsubcategories.create!(name: "Motels")
sub_travel.subsubcategories.create!(name: "Hostal")
sub_travel.subsubcategories.create!(name: "Travel agencies")
sub_travel.subsubcategories.create!(name: "Cruises")
sub_travel.subsubcategories.create!(name: "Theme parks")
sub_travel.subsubcategories.create!(name: "Rural tourism")
sub_travel.subsubcategories.create!(name: "Holiday cottages")
sub_travel.subsubcategories.create!(name: "Carnivals")
sub_travel.subsubcategories.create!(name: "Traditional festivals")
#--------------------Sub Nightlife--------------------
sub_nightlife = enter.subcategories.create!(name: "Nightlife")

sub_nightlife.subsubcategories.create!(name: "Discotheques")
sub_nightlife.subsubcategories.create!(name: "Pubs")
sub_nightlife.subsubcategories.create!(name: "Nightclubs")
#--------------------Sub Gambling--------------------
sub_gambling = enter.subcategories.create!(name: "Gambling")

sub_gambling.subsubcategories.create!(name: "Casinos")
sub_gambling.subsubcategories.create!(name: "Bingo")
sub_gambling.subsubcategories.create!(name: "Lotteries")
#--------------------Sub Humor--------------------
sub_humor = enter.subcategories.create!(name: "Humor")

sub_humor.subsubcategories.create!(name: "Humorists")
sub_humor.subsubcategories.create!(name: "Comedians")
sub_humor.subsubcategories.create!(name: "Circus")
#--------------------Sub Magic--------------------
sub_magic = enter.subcategories.create!(name: "Magic")

sub_magic.subsubcategories.create!(name: "Magicians")
sub_magic.subsubcategories.create!(name: "Sorcerers")
sub_magic.subsubcategories.create!(name: "Tarot")
#--------------------Sub Art--------------------
sub_art = enter.subcategories.create!(name: "Art")

sub_art.subsubcategories.create!(name: "Painting")
sub_art.subsubcategories.create!(name: "Crafts")
sub_art.subsubcategories.create!(name: "sculpture")
sub_art.subsubcategories.create!(name: "Galleries")
sub_art.subsubcategories.create!(name: "Museums")
sub_art.subsubcategories.create!(name: "Antique shops")

#------------------------------------------------------------
#--------------------Indistries SUBS-------------------------
#------------------------------------------------------------
sub_constru = indus.subcategories.create!(name: "Construction")
sub_cien = indus.subcategories.create!(name: "Ciencia")
sub_telec = indus.subcategories.create!(name: "Telecommunications")
sub_min = indus.subcategories.create!(name: "Mining")
sub_metal = indus.subcategories.create!(name: "Metallurgy")
sub_text = indus.subcategories.create!(name: "Textile")
sub_food = indus.subcategories.create!(name: "Food")
sub_car = indus.subcategories.create!(name: "Cars")
sub_serv = indus.subcategories.create!(name: "Services")
sub_aero = indus.subcategories.create!(name: "Aerospace")
sub_sci = indus.subcategories.create!(name: "Science")
sub_petrov = indus.subcategories.create!(name: "Petrochemical")
sub_pharma = indus.subcategories.create!(name: "Pharmaceutical")
sub_agri = indus.subcategories.create!(name: "Agribusiness")
sub_robo = indus.subcategories.create!(name: "Robotics")
sub_it = indus.subcategories.create!(name: "IT")
sub_chem = indus.subcategories.create!(name: "Chemical")
sub_press =indus.subcategories.create!(name: "Press")
sub_ener = indus.subcategories.create!(name: "Energy")

sub_trans = indus.subcategories.create!(name: "Transportation")

#--------------------Sub construc--------------------
ss_real_state = sub_constru.subsubcategories.create!(name: "Real estate")
sub_constru.subsubcategories.create!(name: "Rentals")
sub_constru.subsubcategories.create!(name: "Sales")
sub_constru.subsubcategories.create!(name: "Renovation")
sub_constru.subsubcategories.create!(name: "Construction")
sub_constru.subsubcategories.create!(name: "Construction of buildings")
sub_constru.subsubcategories.create!(name: "Stores materials")
sub_constru.subsubcategories.create!(name: "maintenance")
#--------------------Sub construc--------------------
sub_cien.subsubcategories.create!(name: "Nano particles")
sub_cien.subsubcategories.create!(name: "Particle Accelerator")
sub_cien.subsubcategories.create!(name: "Time travel")
sub_cien.subsubcategories.create!(name: "Astronomy")
sub_cien.subsubcategories.create!(name: "Oservatories")
sub_cien.subsubcategories.create!(name: "Traveling to cosmos")
#--------------------Sub mine--------------------

sub_min.subsubcategories.create!(name: "Mining companies")
sub_min.subsubcategories.create!(name: "Shops")
sub_min.subsubcategories.create!(name: "Transport")
sub_min.subsubcategories.create!(name: "Supplies")
sub_min.subsubcategories.create!(name: "Miners")
#--------------------Sub Metallurgy--------------------
sub_metal.subsubcategories.create!(name: "Metallurgical enterprises")
sub_metal.subsubcategories.create!(name: "Transport")
sub_metal.subsubcategories.create!(name: "Distributors")
sub_metal.subsubcategories.create!(name: "jewelry")
sub_metal.subsubcategories.create!(name: "Gold & Silver")
#--------------------Sub Textile--------------------
sub_text.subsubcategories.create!(name: "Fashion")
sub_text.subsubcategories.create!(name: "Trends")
sub_text.subsubcategories.create!(name: "Designers")
sub_text.subsubcategories.create!(name: "Tailors")
sub_text.subsubcategories.create!(name: "Manufacturers")
sub_text.subsubcategories.create!(name: "Wholesalers")
sub_text.subsubcategories.create!(name: "Scrap")
sub_text.subsubcategories.create!(name: "Supply stores")
#--------------------Sub food--------------------
sub_food.subsubcategories.create!(name: "Dealers")
sub_food.subsubcategories.create!(name: "Drinks and liquors")
sub_food.subsubcategories.create!(name: "Supermarkets")
sub_food.subsubcategories.create!(name: "Restaurants ")
sub_food.subsubcategories.create!(name: "Coffe")
sub_food.subsubcategories.create!(name: "Bars")
sub_food.subsubcategories.create!(name: "Farmers")
sub_food.subsubcategories.create!(name: "Markets")
sub_food.subsubcategories.create!(name: "Catering")
sub_food.subsubcategories.create!(name: "Fishmongers")
sub_food.subsubcategories.create!(name: "Butchers")
sub_food.subsubcategories.create!(name: "Bakeries")
sub_food.subsubcategories.create!(name: "Pastry shops")
#--------------------Sub Car--------------------
sub_car.subsubcategories.create!(name: "Manufacturers")
sub_car.subsubcategories.create!(name: "Dealers")
sub_car.subsubcategories.create!(name: "Mechanics")
sub_car.subsubcategories.create!(name: "Scrap")
sub_car.subsubcategories.create!(name: "Sheet metal")
sub_car.subsubcategories.create!(name: "Paint")
sub_car.subsubcategories.create!(name: "Car rent")
#sub_car.subsubcategories.create!(name: "")
#sub_car.subsubcategories.create!(name: "")
#--------------------Sub Services--------------------
sub_serv.subsubcategories.create!(name: "Manufacture")
sub_serv.subsubcategories.create!(name: "Lawyers")
sub_serv.subsubcategories.create!(name: "Managers/Accountants")
sub_serv.subsubcategories.create!(name: "Notaries")
#--------------------Sub Aerospace--------------------
sub_aero.subsubcategories.create!(name: "Aircraft manufacture")
sub_aero.subsubcategories.create!(name: "Airlines")
sub_aero.subsubcategories.create!(name: "Travel agencies")
#--------------------Sub Science--------------------
sub_sci.subsubcategories.create!(name: "Nano particles")
sub_sci.subsubcategories.create!(name: "Travel into space")
sub_sci.subsubcategories.create!(name: "Particle accelerator")
sub_sci.subsubcategories.create!(name: "Time travel")
sub_sci.subsubcategories.create!(name: "Astronomy")
sub_sci.subsubcategories.create!(name: "Observatories")
#--------------------Sub petro--------------------
sub_petrov.subsubcategories.create!(name: "Refineries")
sub_petrov.subsubcategories.create!(name: "Oil tankers")
sub_petrov.subsubcategories.create!(name: "Gas stations")
sub_petrov.subsubcategories.create!(name: "Extraction of crude")
sub_petrov.subsubcategories.create!(name: "Derivatives")
#--------------------Sub pharma--------------------
sub_pharma.subsubcategories.create!(name: "Pharmacies")
sub_pharma.subsubcategories.create!(name: "Pharmaceutical manufacturers")
sub_pharma.subsubcategories.create!(name: "Dealers")
#--------------------Sub pharma--------------------
sub_agri.subsubcategories.create!(name: "Farms")
sub_agri.subsubcategories.create!(name: "Poultry farms")
sub_agri.subsubcategories.create!(name: "Farmers")
sub_agri.subsubcategories.create!(name: "Peasants")
sub_agri.subsubcategories.create!(name: "Pig farms")
sub_agri.subsubcategories.create!(name: "Horse stables")
sub_agri.subsubcategories.create!(name: "Cooperatives")
sub_agri.subsubcategories.create!(name: "Pig farms")
sub_agri.subsubcategories.create!(name: "Fishing")
#--------------------Sub robotic--------------------
sub_robo.subsubcategories.create!(name: "Manufacturers")
sub_robo.subsubcategories.create!(name: "Dealers / Shops")
sub_robo.subsubcategories.create!(name: "Engineers")
sub_robo.subsubcategories.create!(name: "Technicians")
#--------------------Sub robotic--------------------
sub_it.subsubcategories.create!(name: "Computer manufacturers")
sub_it.subsubcategories.create!(name: "Software engineers")
sub_it.subsubcategories.create!(name: "Computer technicians")
sub_it.subsubcategories.create!(name: "Web designers")
sub_it.subsubcategories.create!(name: "Freelance")
#--------------------Sub Energy--------------------
sub_ener.subsubcategories.create!(name: "Thermoelectric")
sub_ener.subsubcategories.create!(name: "Hydroelectric")
sub_ener.subsubcategories.create!(name: "Eolic")
sub_ener.subsubcategories.create!(name: "Solar")
sub_ener.subsubcategories.create!(name: "Wind")
#--------------------Sub Telecommunications--------------------
sub_telec.subsubcategories.create!(name: "Telecommunications companies")
sub_telec.subsubcategories.create!(name: "Phone manufacturers")
sub_telec.subsubcategories.create!(name: "Cable manufacturers")
sub_telec.subsubcategories.create!(name: "Dealers")
sub_telec.subsubcategories.create!(name: "Telecommunications engineers")
sub_telec.subsubcategories.create!(name: "technicians")
sub_telec.subsubcategories.create!(name: "Service companies")
sub_telec.subsubcategories.create!(name: "Dealers")
#--------------------Sub Transportation--------------------
sub_trans.subsubcategories.create!(name: "Transport companies")
sub_trans.subsubcategories.create!(name: "Public transport")
sub_trans.subsubcategories.create!(name: "Bus terminals")
sub_trans.subsubcategories.create!(name: "Train terminals")
sub_trans.subsubcategories.create!(name: "shipping")
sub_trans.subsubcategories.create!(name: "Boat manufacturers")
sub_trans.subsubcategories.create!(name: "Taxis ")
sub_trans.subsubcategories.create!(name: "Maintenance and cleaning")
#--------------------Sub Chemical--------------------
sub_chem.subsubcategories.create!(name: "Fertilizer manufacturers")
sub_chem.subsubcategories.create!(name: "household products")
#--------------------Sub Chemical--------------------
sub_press.subsubcategories.create!(name: "Writers")
sub_press.subsubcategories.create!(name: "Magazines")
sub_press.subsubcategories.create!(name: "Retailers")
sub_press.subsubcategories.create!(name: "Newspapers")
sub_press.subsubcategories.create!(name: "Recycled")
#------------------------------------------------------------
#it should be only a category for sports
#------------------------------------------------------------
#------------------------------------------------------------
# Educacion
#------------------------------------------------------------
edu_sub_techer = edu.subcategories.create!(name: "Teacher")
edu.subcategories.create!(name: "Professors")
edu.subcategories.create!(name: "Childhood Education")
edu.subcategories.create!(name: "High school")
edu.subcategories.create!(name: "University")
edu.subcategories.create!(name: "Academies")
edu.subcategories.create!(name: "Faculties")
edu.subcategories.create!(name: "Postgraduates")
edu.subcategories.create!(name: "Children")
edu.subcategories.create!(name: "Nursery")
edu.subcategories.create!(name: "primary education")
edu.subcategories.create!(name: "Secondary education")
edu.subcategories.create!(name: "Colleges")
edu.subcategories.create!(name: "Graduate")
edu.subcategories.create!(name: "Distribution stores")
#Medicina general, (añadir subcategorías de, doctores y especialistas, enfermeras, hospitales, clínicas . dentro de cada especialidad de medicina].Medicina interna, radiología, oftalmología, pediatría, anestesiología,                                                                                          #, , formula 1, carreras de coches,
#------------------------------------------------------------
# Medicine Sucategory TODO
#------------------------------------------------------------
sub_med_g = heal_beauty.subcategories.create!(name: "General medicine")
sub_med_vet = heal_beauty.subcategories.create!(name: "Veterinary medicine")
sub_med_alt = heal_beauty.subcategories.create!(name: "Alternative medicine,")
#--------------------Sub Vet-------------------
sub_med_vet.subsubcategories.create!(name: "Clinics")
sub_med_vet.subsubcategories.create!(name: "Farms host")
sub_med_vet.subsubcategories.create!(name: "Pet shops")
sub_med_vet.subsubcategories.create!(name: "Pet foods")
sub_med_vet.subsubcategories.create!(name: "Animal trainers")
#--------------------Sub Vet-------------------
sub_med_alt.subsubcategories.create!(name: "Pharmacies of natural products")
sub_med_alt.subsubcategories.create!(name: "Massage")
sub_med_alt.subsubcategories.create!(name: "Acupuncture")
sub_med_alt.subsubcategories.create!(name: "Reiki")
sub_med_alt.subsubcategories.create!(name: "Midwives")
sub_med_alt.subsubcategories.create!(name: "Tai chi")
sub_med_alt.subsubcategories.create!(name: "Naturopathic doctors")
#------------------------------------------------------------
# heal_beauty_beauty Sucategory
#------------------------------------------------------------
bty_salon_sub = heal_beauty.subcategories.create!(name: "Beauty salons")
heal_beauty.subcategories.create!(name: "Esthetic clinics")
heal_beauty.subcategories.create!(name: "Health Food clinics")
heal_beauty.subcategories.create!(name: "Health")
heal_beauty.subcategories.create!(name: "Nutrition")
#------------------------------------------------------------
# Foundations Sucategory
#-----------------------------------------------------------
sub_relg = fund.subcategories.create!(name: "Religions")
sub_relg.subsubcategories.create!(name: "Catholic")
sub_relg.subsubcategories.create!(name: "Pentecostal")

sub_relg.subsubcategories(name: "Jehovah's Witnesses")
sub_relg.subsubcategories(name: "Buddhism")
sub_relg.subsubcategories(name: "Muslims")
sub_relg.subsubcategories(name: "Jews")
sub_relg.subsubcategories(name: "Adventists")
sub_relg.subsubcategories(name: "Christianity")
sub_relg.subsubcategories(name: "Hinduism")
sub_relg.subsubcategories(name: "neo paganism")
sub_relg.subsubcategories(name: "African American")
sub_relg.subsubcategories(name: "Shamanism")
sub_relg.subsubcategories(name: "Judaism")
sub_relg.subsubcategories(name: "Jainism")
sub_relg.subsubcategories(name: "Shinto")
sub_relg.subsubcategories(name: "Chinese folk religion")
sub_relg.subsubcategories(name: "Rastafarianism")
sub_relg.subsubcategories(name: "Gnosticism")
sub_relg.subsubcategories(name: "Sikhism")
sub_relg.subsubcategories(name: "Zoroastrianism")
sub_relg.subsubcategories(name: "Baha'i")
sub_relg.subsubcategories(name: "Unitarian Universalism")
sub_relg.subsubcategories(name: "Caodaism")
sub_relg.subsubcategories(name: "Masonic")
sub_relg.subsubcategories(name: "hermetic")
#Nonprofit foundations

sub_noproft = fund.subcategories.create!(name: "NGO's")

sub_pltics = fund.subcategories.create!(name: "Politics")
sub_pltics.subsubcategories.create!(name: "Socialists")
sub_pltics.subsubcategories.create!(name: "Democrats")
sub_pltics.subsubcategories.create!(name: "Republicans")
sub_pltics.subsubcategories.create!(name: "Communists")
sub_pltics.subsubcategories.create!(name: "Popular")
sub_pltics.subsubcategories.create!(name: "Center")
sub_pltics.subsubcategories.create!(name: "Left")
sub_pltics.subsubcategories.create!(name: "Rigth")


99.times do |n|
  name = Faker::Name.name
  # email = "example-#{n+1}@sttattus.org"
  email = "example-#{rand(100000)}@sttattus.org"
  password = "password"
  phone = 1212
  country = "Venezuela"
  state = "Merida"
  birthday = '28/10/2011'
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               subcategory_id: sub_music.id,
               phone: phone,
               country: country,
               state: state,
               birthdate: birthday,

  )

end

#creando post a los usuarios
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.posts.create!(content: content) }
end


#follow relationships
users = User.all
user = users.first

following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
following.each { |follower| follower.follow(user) }

user_enter = User.create!(name: "Example User",
                          email: "example@sttattus.org",
                          password: "foobar",
                          password_confirmation: "foobar",
                          subcategory_id: sub_music.id,
                          phone: 1212,
                          country: "Venezuela",
                          state: "Merida",
                          birthdate: '28/10/2011',
                          admin: true)

user_enter.posts.create!(
    content: "First User post",
    picture: File.open(Rails.root + "app/assets/images/Pyramide_Kheops.JPG"))
user_enter.posts.create!(
    content: "second user post",
    picture: File.open(Rails.root + "app/assets/images/chuto.jpg"))
user_enter.posts.create!(
    content: "third user post",
    picture: File.open(Rails.root + "app/assets/images/jeroglificos-de-egipto.jpg"))

user_enter.posts.create!(
    content: "4 user post",
    picture: File.open(Rails.root + "app/assets/images/chinese.jpg"))

user_enter.posts.create!(
    content: "five user post",
    picture: File.open(Rails.root + "app/assets/images/DEB.jpg"))

user_enter.posts.create!(
    content: "six user post",
    picture: File.open(Rails.root + "app/assets/images/00F.jpg"))


user_enter.posts.create!(
    content: "seven user post",
    picture: File.open(Rails.root + "app/assets/images/lr-13000-3.jpg"))

user_enter.posts.create!(
    content: "eigth user post",
    picture: File.open(Rails.root + "app/assets/images/esfinge3.jpg"))

#user profile pics


user_enter.profile_pictures.create!(picture: File.open(Rails.root + "app/assets/images/publi2.jpg"),
                                    current: true)

user_enter.profile_pictures.create!(picture: File.open(Rails.root + "app/assets/images/publi1.jpg"),
                                    current: false)

user_enter.profile_pictures.create!(picture: File.open(Rails.root + "app/assets/images/publi3.jpg"),
                                    current: false)

user_enter.profile_pictures.create!(picture: File.open(Rails.root + "app/assets/images/mino.jpg"),
                                    current: false)

user_enter.portraits.create!(picture: File.open(Rails.root + "app/assets/images/chinese.jpg"),
                             current: true)


user_indus = User.create!(name: "Example Industry follower",
                          email: "exampleI@sttattus.org",
                          password: "foobar",
                          password_confirmation: "foobar",
                          subcategory_id: sub_constru.id,
                          phone: 1212,
                          country: "Venezuela",
                          state: "Merida",
                          birthdate: '28/10/2011')


user_indus.profile_pictures.create!(picture: File.open(Rails.root + "app/assets/images/publi2.jpg"),
                                    current: true)

user_indus.profile_pictures.create!(picture: File.open(Rails.root + "app/assets/images/publi1.jpg"),
                                    current: false)


user_indus.posts.create!(content: "Construction subcategory post belong_to industry")
user_indus.posts.create!(content: "Construction subcategory post belong_to industry 2")
user_indus.posts.create!(content: "Construction subcategory post belong_to industry 3")


#------------------------------------------------------------

user_edu = User.create!(name: "Example Education follower",
                        email: "exampleEdu@sttattus.org",
                        password: "foobar",
                        password_confirmation: "foobar",
                        subcategory_id: edu_sub_techer.id,
                        phone: 1212,
                        country: "Venezuela",
                        state: "Merida",
                        birthdate: '28/10/2011')

user_edu.posts.create!(content: "Teacher subcategory post belong_to Education")
user_edu.posts.create!(content: "Teacher subcategory post belong_to Education 2")
user_edu.posts.create!(content: "Teacher subcategory post belong_to Education 3")
#------------------------------------------------------------

user_heal_beauty = User.create!(name: "Example heal_beautyth follower",
                                email: "exampleheal_beauty@sttattus.org",
                                password: "foobar",
                                password_confirmation: "foobar",
                                subcategory_id: sub_med_g.id,
                                phone: 1212,
                                country: "Venezuela",
                                state: "Merida",
                                birthdate: '28/10/2011')

user_heal_beauty.posts.create!(content: "General Medicine post belong_to heal_beautyth")
user_heal_beauty.posts.create!(content: "General Medicine post belong_to heal_beautyth 2")
user_heal_beauty.posts.create!(content: "General Medicine post belong_to heal_beautyth 3")

#------------------------------------------------------------

user_fund = User.create!(name: "Example fundation follower",
                         email: "examplefund@sttattus.org",
                         password: "foobar",
                         password_confirmation: "foobar",
                         subcategory_id: sub_relg.id,
                         phone: 1212,
                         country: "Venezuela",
                         state: "Merida",
                         birthdate: '28/10/2011')

user_fund.portraits.create!(picture: File.open(Rails.root + "app/assets/images/chinese.jpg"),
                            current: true)

#user_fund.profile_pictures.create!(picture: File.open(Rails.root + "app/assets/images/publi2.jpg"),
#                                    current: true)


user_fund.posts.create!(content: "Religion post belong_to fundation")
user_fund.posts.create!(content: "Religion post belong_to fundation 2")
user_fund.posts.create!(content: "Religion post belong_to fundation 3")

#------------------------------------------------------------

user_beau = User.create!(name: "Example beauty follower",
                         email: "examplebeau@sttattus.org",
                         password: "foobar",
                         password_confirmation: "foobar",
                         subcategory_id: bty_salon_sub.id,
                         phone: 1212,
                         country: "Venezuela",
                         state: "Merida",
                         birthdate: '28/10/2011')

rafa_user = User.create!(name: "rafael solorzano",
                          email: "rafa.a.solorzano@gmail.com",
                          password: "foobar",
                          password_confirmation: "foobar",
                          subcategory_id: sub_constru.id,
                          phone: 1212,
                          country: "Venezuela",
                          state: "Merida",
                          birthdate: '28/10/2011')

user_beau.posts.create!(content: "beauty salon post belong_to Beaty")
user_beau.posts.create!(content: "heal_beauty_beauty salon post belong_to Beaty 2")
user_beau.posts.create!(content: "heal_beauty_beauty salon post belong_to Beaty 3")

#------------------------------------------------------------

ProductCart.delete_all
product_1 = ProductCart.create! id: 1, name: "Construction1", price: 0.49, active: true, user_id: user_indus.id, subcategory_id: sub_constru.id
product_2 = ProductCart.create! id: 2, name: "Construction2", price: 0.49, active: true, user_id: user_indus.id, subcategory_id: sub_constru.id
product_3 = ProductCart.create! id: 3, name: "Constructition3", price: 0.49, active: true, user_id: user_indus.id, subcategory_id: sub_constru.id
product_4 = ProductCart.create! id: 4, name: "Music1", price: 0.29, active: true, user_id: user_enter.id, subcategory_id: sub_music.id
product_5 = ProductCart.create! id: 5, name: "Music2", price: 0.29, active: true, user_id: user_enter.id, subcategory_id: sub_music.id
product_6 = ProductCart.create! id: 6, name: "Music3", price: 0.29, active: true, user_id: user_enter.id, subcategory_id: sub_music.id
# product_7 = ProductCart.create! id: 7, name: "Music4", price: 0.29, active: true, user_id: user_enter.id, subcategory_id: sub_music.id, video: File.open(Rails.root + "app/assets/videos/video_1.mp4");
product_7 = ProductCart.create! id: 7, name: "Music4", price: 0.29, active: true, user_id: user_enter.id, subcategory_id: sub_music.id;
product_8 = ProductCart.create! id: 8, name: "Medicine1", price: 1.99, active: true, user_id: user_heal_beauty.id, subcategory_id: sub_med_g.id
product_9 = ProductCart.create! id: 9, name: "Medicine2", price: 1.99, active: true, user_id: user_heal_beauty.id, subcategory_id: sub_med_g.id
product_10 = ProductCart.create! id: 10, name: "Medicine3", price: 1.99, active: true, user_id: user_heal_beauty.id, subcategory_id: sub_med_g.id
product_11 = ProductCart.create! id: 11, name: "Medicine4", price: 1.99, active: true, user_id: user_heal_beauty.id, subcategory_id: sub_med_g.id


product_1.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_1.id)

product_1.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_1.id)

product_1.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_1.id)

product_1.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_1.id)

product_2.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_2.id)

product_2.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_2.id)

product_2.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_2.id)

product_2.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_2.id)

product_3.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_3.id)

product_3.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_3.id)

product_3.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_3.id)

product_3.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_3.id)


product_4.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_4.id)

product_4.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_4.id)

product_4.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_4.id)

product_4.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_4.id)


product_5.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_5.id)

product_5.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_5.id)

product_5.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_5.id)

product_5.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_5.id)


product_6.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_6.id)

product_6.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_6.id)

product_6.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_6.id)

product_6.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_6.id)


product_7.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_7.id)

product_7.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_7.id)

product_7.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_7.id)

product_7.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_7.id)


product_8.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_8.id)

product_8.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_8.id)

product_8.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_8.id)

product_8.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_8.id)


product_9.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                 product_cart_id: product_9.id)

product_9.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                 product_cart_id: product_9.id)

product_9.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                 product_cart_id: product_9.id)

product_9.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                 product_cart_id: product_9.id)


product_10.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                  product_cart_id: product_10.id)

product_10.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                  product_cart_id: product_10.id)

product_10.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                  product_cart_id: product_10.id)

product_10.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                  product_cart_id: product_10.id)


product_11.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_1.jpg"),
                                  product_cart_id: product_11.id)

product_11.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_2.jpg"),
                                  product_cart_id: product_11.id)

product_11.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_3.jpg"),
                                  product_cart_id: product_11.id)

product_11.product_images.create!(picture_product: File.open(Rails.root + "app/assets/images/IMG_4.jpg"),
                                  product_cart_id: product_11.id)


OrderStatus.delete_all
OrderStatus.create! id: 1, name: "In Progress"
OrderStatus.create! id: 2, name: "Placed"
OrderStatus.create! id: 3, name: "Shipped"
OrderStatus.create! id: 4, name: "Cancelled"

