# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150819151610) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "street_address"
    t.string   "phone"
    t.string   "zip_code"
    t.integer  "country_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "type"
  end

  add_index "addresses", ["country_id"], name: "index_addresses_on_country_id", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comment_products", force: :cascade do |t|
    t.string   "comment"
    t.integer  "product_cart_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
    t.string   "title"
  end

  add_index "comment_products", ["product_cart_id"], name: "index_comment_products_on_product_cart_id", using: :btree
  add_index "comment_products", ["user_id"], name: "index_comment_products_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "content"
    t.integer  "post_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "connected_users", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "connected_users", ["user_id"], name: "index_connected_users_on_user_id", using: :btree

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "conversations", ["recipient_type", "recipient_id"], name: "index_conversations_on_recipient_type_and_recipient_id", using: :btree
  add_index "conversations", ["sender_type", "sender_id"], name: "index_conversations_on_sender_type_and_sender_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "conversation_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "order_items", force: :cascade do |t|
    t.integer  "product_cart_id"
    t.integer  "order_id"
    t.decimal  "unit_price",      precision: 12, scale: 3
    t.integer  "quantity"
    t.decimal  "total_price",     precision: 12, scale: 3
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "order_items", ["order_id"], name: "index_order_items_on_order_id", using: :btree
  add_index "order_items", ["product_cart_id"], name: "index_order_items_on_product_cart_id", using: :btree

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.decimal  "subtotal",         precision: 12, scale: 3
    t.decimal  "tax",              precision: 12, scale: 3
    t.decimal  "shipping",         precision: 12, scale: 3
    t.decimal  "total",            precision: 12, scale: 3
    t.integer  "order_status_id"
    t.integer  "user_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "delivery_company"
    t.string   "payment"
    t.integer  "address_id"
  end

  add_index "orders", ["address_id"], name: "index_orders_on_address_id", using: :btree
  add_index "orders", ["order_status_id"], name: "index_orders_on_order_status_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "portraits", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "picture"
    t.boolean  "current"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "portraits", ["user_id"], name: "index_portraits_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "picture"
    t.string   "video"
    t.boolean  "success"
  end

  add_index "posts", ["user_id", "created_at"], name: "index_posts_on_user_id_and_created_at", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "product_carts", force: :cascade do |t|
    t.string   "name"
    t.decimal  "price",                              precision: 12, scale: 3
    t.boolean  "active"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.integer  "user_id"
    t.string   "video"
    t.boolean  "success"
    t.integer  "subcategory_id"
    t.string   "type_sale"
    t.string   "file_product"
    t.string   "rent_time"
    t.string   "type_product"
    t.decimal  "domestic_delivery"
    t.decimal  "international_delivery"
    t.string   "origin_country"
    t.decimal  "price_for_extra_unit_domestic"
    t.decimal  "price_for_extra_unit_international"
  end

  add_index "product_carts", ["subcategory_id"], name: "index_product_carts_on_subcategory_id", using: :btree
  add_index "product_carts", ["user_id"], name: "index_product_carts_on_user_id", using: :btree

  create_table "product_images", force: :cascade do |t|
    t.string   "picture_product"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "product_cart_id"
  end

  add_index "product_images", ["product_cart_id"], name: "index_product_images_on_product_cart_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.integer  "stock"
    t.float    "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profile_pictures", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "picture"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "current"
  end

  add_index "profile_pictures", ["user_id"], name: "index_profile_pictures_on_user_id", using: :btree

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "relationships", ["followed_id"], name: "index_relationships_on_followed_id", using: :btree
  add_index "relationships", ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true, using: :btree
  add_index "relationships", ["follower_id"], name: "index_relationships_on_follower_id", using: :btree

  create_table "sales", force: :cascade do |t|
    t.decimal  "subtotal"
    t.decimal  "tax"
    t.decimal  "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
  end

  add_index "sales", ["user_id"], name: "index_sales_on_user_id", using: :btree

  create_table "subcategories", force: :cascade do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "subcategories", ["category_id"], name: "index_subcategories_on_category_id", using: :btree

  create_table "subsubcategories", force: :cascade do |t|
    t.string   "name"
    t.integer  "subcategory_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "subsubcategories", ["subcategory_id"], name: "index_subsubcategories_on_subcategory_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin"
    t.string   "id_for_call"
    t.date     "birthdate"
    t.string   "country"
    t.string   "phone"
    t.string   "state"
    t.integer  "subcategory_id"
    t.string   "category"
    t.boolean  "connected"
  end

  add_index "users", ["subcategory_id"], name: "index_users_on_subcategory_id", using: :btree

  add_foreign_key "addresses", "countries"
  add_foreign_key "comment_products", "product_carts"
  add_foreign_key "comment_products", "users"
  add_foreign_key "comments", "posts"
  add_foreign_key "comments", "users"
  add_foreign_key "connected_users", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_items", "product_carts"
  add_foreign_key "orders", "addresses"
  add_foreign_key "orders", "order_statuses"
  add_foreign_key "orders", "users"
  add_foreign_key "portraits", "users"
  add_foreign_key "posts", "users"
  add_foreign_key "product_carts", "subcategories"
  add_foreign_key "product_carts", "users"
  add_foreign_key "product_images", "product_carts"
  add_foreign_key "profile_pictures", "users"
  add_foreign_key "sales", "users"
  add_foreign_key "subcategories", "categories"
  add_foreign_key "subsubcategories", "subcategories"
  add_foreign_key "users", "subcategories"
end
