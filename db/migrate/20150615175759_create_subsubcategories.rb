class CreateSubsubcategories < ActiveRecord::Migration
  def change
    create_table :subsubcategories do |t|
      t.string :name
      t.references :subcategory, index: true

      t.timestamps null: false
    end
    add_foreign_key :subsubcategories, :subcategories
  end
end
