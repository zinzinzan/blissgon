class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :first_name
      t.string :last_name
      t.string :street_address
      t.string :phone
      t.string :zip_code
      t.references :country, index: true

      t.timestamps null: false
    end
    add_foreign_key :addresses, :countries
  end
end
