class AddUserToSale < ActiveRecord::Migration
  def change
    add_reference :sales, :user, index: true
    add_foreign_key :sales, :users
  end
end
