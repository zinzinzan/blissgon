class AddTypeProductToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :type_product, :string
  end
end
