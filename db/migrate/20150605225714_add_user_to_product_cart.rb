class AddUserToProductCart < ActiveRecord::Migration
  def change
    add_reference :product_carts, :user, index: true
    add_foreign_key :product_carts, :users
  end
end
