class AddProductCartToProductImage < ActiveRecord::Migration
  def change
    add_reference :product_images, :product_cart, index: true
    add_foreign_key :product_images, :product_carts
  end
end
