class AddUserToCommentProduct < ActiveRecord::Migration
  def change
    add_reference :comment_products, :user, index: true
    add_foreign_key :comment_products, :users
  end
end
