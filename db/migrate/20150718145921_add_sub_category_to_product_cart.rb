class AddSubCategoryToProductCart < ActiveRecord::Migration
  def change
    add_reference :product_carts, :subcategory, index: true
    add_foreign_key :product_carts, :subcategories
  end
end
