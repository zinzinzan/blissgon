class AddInternationalDeliveryToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :international_delivery, :decimal
  end
end
