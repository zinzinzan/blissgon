class AddFileProductToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :file_product, :string
  end
end
