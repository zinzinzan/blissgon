class AddOriginCountryToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :origin_country, :string
  end
end
