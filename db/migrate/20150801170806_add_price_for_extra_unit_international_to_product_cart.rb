class AddPriceForExtraUnitInternationalToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :price_for_extra_unit_international, :decimal
  end
end
