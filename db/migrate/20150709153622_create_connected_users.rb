class CreateConnectedUsers < ActiveRecord::Migration
  def change
    create_table :connected_users do |t|
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :connected_users, :users
  end
end
