class AddPriceForExtraUnitDomesticToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :price_for_extra_unit_domestic, :decimal
  end
end
