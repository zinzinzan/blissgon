class AddRentTimeToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :rent_time, :string
  end
end
