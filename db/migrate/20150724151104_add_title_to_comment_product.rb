class AddTitleToCommentProduct < ActiveRecord::Migration
  def change
    add_column :comment_products, :title, :string
  end
end
