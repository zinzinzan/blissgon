class AddCurrentToProfilePicture < ActiveRecord::Migration
  def change
    add_column :profile_pictures, :current, :boolean
  end
end
