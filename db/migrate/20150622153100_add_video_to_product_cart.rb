class AddVideoToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :video, :string
  end
end
