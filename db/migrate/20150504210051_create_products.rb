class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :code
      t.integer :stock
      t.float :price

      t.timestamps null: false
    end
  end
end
