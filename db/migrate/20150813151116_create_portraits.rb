class CreatePortraits < ActiveRecord::Migration
  def change
    create_table :portraits do |t|
      t.references :user, index: true
      t.string :picture
      t.boolean :current

      t.timestamps null: false
    end
    add_foreign_key :portraits, :users
  end
end
