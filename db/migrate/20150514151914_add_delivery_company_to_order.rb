class AddDeliveryCompanyToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :delivery_company, :string
  end
end
