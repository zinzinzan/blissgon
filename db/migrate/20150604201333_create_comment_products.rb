class CreateCommentProducts < ActiveRecord::Migration
  def change
    create_table :comment_products do |t|
      t.string :comment
      t.references :product_cart, index: true

      t.timestamps null: false
    end
    add_foreign_key :comment_products, :product_carts
  end
end
