class AddTypeSaleToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :type_sale, :string
  end
end
