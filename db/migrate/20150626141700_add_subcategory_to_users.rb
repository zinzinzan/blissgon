class AddSubcategoryToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :subcategory, index: true
    add_foreign_key :users, :subcategories
  end
end
