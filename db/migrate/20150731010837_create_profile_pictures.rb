class CreateProfilePictures < ActiveRecord::Migration
  def change
    create_table :profile_pictures do |t|
      t.references :user, index: true
      t.string :picture

      t.timestamps null: false
    end
    add_foreign_key :profile_pictures, :users
  end
end
