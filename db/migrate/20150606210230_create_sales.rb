class CreateSales < ActiveRecord::Migration
  def change
    create_table :sales do |t|
      t.decimal :subtotal
      t.decimal :tax
      t.decimal :total

      t.timestamps null: false
    end
  end
end
