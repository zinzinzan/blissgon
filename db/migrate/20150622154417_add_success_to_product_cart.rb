class AddSuccessToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :success, :boolean
  end
end
