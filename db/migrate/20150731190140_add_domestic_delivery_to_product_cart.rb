class AddDomesticDeliveryToProductCart < ActiveRecord::Migration
  def change
    add_column :product_carts, :domestic_delivery, :decimal
  end
end
