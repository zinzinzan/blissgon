require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @category = categories(:enter)
    @sub_category = Subcategory.create!(name: "music", category_id: @category.id )
    @user = User.new(name: "Example User", email: "user@example.com",
                     password: "password", password_confirmation: "password",
                     subcategory_id: @sub_category.id, birthdate: Time.now.strftime('%Y-%m-%d'),
                     country: "usa",  state: "alabama",
                     phone: 12345678 )
    @user.save

  end

  #------------------------------------------------------------

   test "should redirect to login path if no logged" do
     get root_path
     assert_redirected_to login_path
     follow_redirect!
     post login_path, session:  { email: @user.email, password: 'password' }
     assert is_logged_in?
     assert_redirected_to root_path
     follow_redirect!
     assert_template 'static_pages/home'
   end

  #------------------------------------------------------------

end