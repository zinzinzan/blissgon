require 'test_helper'

class FollowingTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @other = users(:archer)
    log_in_as(@user)
  end

  test "Should follow" do
    assert_difference '@user.following.count', 1 do
      post relationships_path, followed_id: @other.id;
    end
  end

#------------------------------------------------------

  test "Should unfollow" do
    @user.follow(@other)
    relationship = @user.active_relationships.find_by( followed_id: @other.id )
    assert_difference '@user.following.count', -1 do
      delete relationship_path(relationship);
    end
  end

end
