require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  #------------------------------------------------------

	def setup
    @category = categories(:enter)
    @sub_category = Subcategory.create!(name: "music", category_id: @category.id )
    @user = User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar",
                     subcategory_id: @sub_category.id, birthdate: Time.now.strftime('%Y-%m-%d'),
                     country: "usa",  state: "alabama",
                     phone: 12345678 )
    @user.save
  end

  #------------------------------------------------------

	test "unsuccessfull edit" do
		log_in_as(@user)
=begin
		get edit_user_path(@user)
		assert_template 'users/edit'
		patch user_path(@user), user: { name: "",
																		email: "foo@invalid",
																		password: "foo", 
																		password_confirmation: "bar",

																	}  
		assert_template 'users/edit'
=end
																	
	end

  #------------------------------------------------------

	test "successful edit" do
=begin
		@category = categories(:enter)
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name  = "Foo Bar"
    email = "foo@bar.com"
    category_id = @category.id
    patch user_path(@user), user: { name:  name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "",
                                    category_id: category_id,
                                    birthdate: Time.now.strftime('%Y-%m-%d'),
                                    country: "usa" ,
                                    state: "alabama",
                                    phone: 123456
                                    }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal @user.name,  name
    assert_equal @user.email, email
=end
  end

  #------------------------------------------------------

end
