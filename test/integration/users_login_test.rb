require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @category = categories(:enter)
    @sub_category = Subcategory.create!(name: "music", category_id: @category.id )
    @user = User.new(name: "Example User", email: "user@example.com",
                     password: "password", password_confirmation: "password",
                     subcategory_id: @sub_category.id, birthdate: Time.now.strftime('%Y-%m-%d'),
                     country: "usa",  state: "alabama",
                     phone: 12345678 )

    @user.save
  end


  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: { email: "", password: "" }
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end


  test "login with valid information followed by logout" do
    get login_path
    post login_path, session:  { email: @user.email, password: 'password' }
    assert is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_template 'shared/_post_form'
    assert_template 'shared/_feed'
    assert_template 'static_pages/home'
    assert_template 'layouts/_shim'
    assert_template 'shared/_sub_categories'
    assert_template 'shared/_navbar_top'
    assert_template 'layouts/_header'

    #assert_template 'shared/right'
    assert_select "a[href=?]", login_path, count:0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    #delete logout_path
    assert_redirected_to login_path
    follow_redirect!
    assert_template 'sessions/new'
  end
  
	test "test login with remebering" do
		log_in_as(@user, remember_me: '1')
		assert_not_nil cookies['remember_token']
	end
	
	test "test loging without remembering" do
		log_in_as(@user, remember_me: '0')
		assert_nil cookies['remember_token']
	end

end
