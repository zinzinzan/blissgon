require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest

  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: { name:  "",
                               email: "user@invalid",
                               password:              "foo",
                               password_confirmation: "bar",
                               subcategory_id: nil
                              }
    end
    assert_template 'users/new'
  end
  
  test "valid signup information" do
    @category = categories(:enter)
    @sub_category = Subcategory.new(name: "music", category_id: @category.id )
    @sub_category.save
    get signup_path
  	assert_difference 'User.count', 1 do
  		post_via_redirect users_path,
                        user: { name:  "whatever",
                                email: "user@valid.com",
                                password:              "111111",
                                password_confirmation: "111111",
                                subcategory_id: @sub_category.id,
                                birthdate: Time.now.strftime('%Y-%m-%d'),
                                country: "usa",
                                state: "alabama",
                                phone: 12345678
                               }
  	end 
  		assert_template 'static_pages/home'
  		assert is_logged_in?
  end

end