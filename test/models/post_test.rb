require 'test_helper'

class PostTest < ActiveSupport::TestCase

#------------------------------------------------------------

  def setup
    @user = users(:michael)
    @post =  @user.posts.build(content: "lorem ipsum" )
    @other = User.new(name: "Example User", email: "user@example.com",
                       password: "foobar", password_confirmation: "foobar")
  end

#------------------------------------------------------------

  test "should be valid" do
    assert @post.valid?
  end

#------------------------------------------------------------

  test "user id should be present" do
    @post.user_id = nil
    assert_not @post.valid?
  end

#------------------------------------------------------------

  test "content should by present" do
    @post.content = "    ";
    assert_not @post.valid?
  end

#------------------------------------------------------------

  test "content should be at most 2024" do
    @post.content = "a" * 2025;
    assert_not @post.valid?
  end

#------------------------------------------------------------

  test "order should be most recent first" do
    assert_equal Post.first, posts(:most_recent)
  end

#------------------------------------------------------------

  #test "associated comments should be destroyed" do
   # other.save

  #end

end
