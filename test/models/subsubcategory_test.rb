require 'test_helper'

class SubsubcategoryTest < ActiveSupport::TestCase

  #------------------------------------------------------

  def setup
    @category = categories(:enter)
    @sub_category = Subcategory.new(name: "music", category_id: @category.id)
    @subSub_category = Subsubcategory.create!(name: "music sotre", subcategory_id: @sub_category.id)
    @user = users(:michael)
  end

  #------------------------------------------------------

  test "should be valid" do
    assert @sub_category.valid?
  end

  #------------------------------------------------------

  test "should require a category_id" do
    @sub_category.category_id = nil
    assert_not @sub_category.valid?
  end

  #------------------------------------------------------

end

