require 'test_helper'

class CommentTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @post = @user.posts.create(content: "post comment!")
    #@comment = @user.comments.build(content: "first")
    @comment = @post.comments.create!(content: "firt comment",user_id: @user.id);
    #@comment = Comment.create!(content: "Lorem .... ipsum", user_id: @user.id, post_id: @post.id)

  end

  test "should be valid" do
    assert @comment.valid?
  end

  test "shold havea user_id valid" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end
  test "shold have a post_id valid " do
    @comment.post_id = nil
    assert_not @comment.valid?
  end

  test "should be present" do
    @comment.content = "  "
    assert_not @comment.valid?
  end

  test "content should have max 512chars" do
    @comment.content = 'a'*513;
    assert_not @comment.valid?
  end


  test "associated micropost should be destroyed" do
    @comment
  end

end
