require 'test_helper'

class UserTest < ActiveSupport::TestCase

  #------------------------------------------------------

  def setup
    @category = categories(:enter)
    @sub_category = Subcategory.create!(name: "music", category_id: @category.id )
    @user = User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar",
                     subcategory_id: @sub_category.id, birthdate: Time.now.strftime('%Y-%m-%d'),
                     country: "usa",  state: "alabama",
                     phone: 12345678 )
  end
  #------------------------------------------------------

  test "should be valid" do
    assert @user.valid?
  end

  #------------------------------------------------------

  test "name is present?" do
    @user.name = " ";
    assert_not @user.valid?
  end

  #------------------------------------------------------

  test "email should be present" do
    @user.email = " ";
    assert_not @user.valid?
  end

  #------------------------------------------------------

  test "subcategory_id should be present" do
    @user.subcategory_id = nil;
    assert_not @user.valid?
  end

  #------------------------------------------------------

  test "birthdate should be present" do
    @user.birthdate = nil;
    assert_not @user.valid?
  end

  #------------------------------------------------------

  test "name should not be too long" do
    @user.name = "a"*51;
    assert_not @user.valid?
  end

  #------------------------------------------------------

  test "email should not be too long" do
    @user.email = "a"*244 + "@example.com" ;
    assert_not @user.valid?
  end

  #------------------------------------------------------

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_USER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be    valid"
    end
  end

  #------------------------------------------------------

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  #------------------------------------------------------

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  #------------------------------------------------------

  test "pass length" do
   @user.password = @user.password_confirmation = "a"*5;
   assert_not @user.valid?
  end

  #------------------------------------------------------

	test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?('')
  end

  #------------------------------------------------------

  test "associated post should be delete when owner is deleted" do
    @user.save
    @post = @user.posts.create(content: "lorem ipsum")
    assert_difference 'Post.count', -1 do
      @user.destroy
    end
  end

  #------------------------------------------------------

  test "associated comments should be delete when owner is deleted" do
    @user.save
    @post = @user.posts.create(content: "lorem ipsum")
    @comment = @post.comments.create(content: "weee", user_id: @user.id )
    assert_difference 'Comment.count', -1 do
      @user.destroy
    end
  end

  #------------------------------------------------------

  test "should follow and un follow user" do
    michael = users(:michael)
    archer = users(:archer)
    assert_not michael.following?(archer)
    michael.follow(archer)
    assert archer.followers.include?(michael)    
    assert michael.following?(archer)
    michael.unfollow(archer)
    assert_not michael.following?(archer)
  end

  #------------------------------------------------------

  test "feed should have the right subcatery posts" do

    @category = categories(:enter)
    @sub_category_msc = Subcategory.create!(name: "music", category_id: @category.id )
    @sub_category_hwy = Subcategory.create!(name: "movie", category_id: @category.id )

    michael = users(:michael)
    archer  = users(:archer)
    lana    = users(:lana)
    mallory = users(:mallory)


    michael.subcategory_id = @sub_category_msc.id
    mallory.subcategory_id = @sub_category_msc.id
    archer.subcategory_id = @sub_category_hwy.id

    michael.save
    mallory.save
    archer.save

    # Posts from followed user
    lana.posts.each do |post_following|
      assert michael.feed(michael.subcategory_id).include?(post_following)
    end
    # Posts from self
    michael.posts.each do |post_self|
      assert michael.feed(michael.subcategory_id).include?(post_self)
    end
    # Posts from unfollowed user
    archer.posts.each do |post_unfollowed|
      assert_not michael.feed(michael.subcategory_id).include?(post_unfollowed)
    end
    # Post from same users in the category
    mallory.posts.each do |post_category|
      assert michael.feed(michael.subcategory_id).include?(post_category)
    end

  end

  #------------------------------------------------------

  test "should have categories and sub categories" do
    @category = categories(:enter)
    @sub_category = Subcategory.new(name: "music", category_id: @category.id )
  end

  #------------------------------------------------------
  test "user Subcategorys should not be duplicated" do
    #TOdO
  end

  #------------------------------------------------------


end