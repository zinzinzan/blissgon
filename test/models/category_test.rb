require 'test_helper'

class CategoryTest < ActiveSupport::TestCase

  #------------------------------------------------------

  def setup
    @category = Category.new(name: "Entretainment" )
  end

  #------------------------------------------------------

  test "shouldbe valid" do
    assert @category.valid?
  end

  #------------------------------------------------------

  test "should require a name" do
    @category.name = nil;
    assert_not @category.valid?
  end

  #------------------------------------------------------

end
