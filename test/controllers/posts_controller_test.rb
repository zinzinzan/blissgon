require 'test_helper'

class PostsControllerTest < ActionController::TestCase

  def setup
    @post=posts(:orange)
  end

  test "should redirect create not logged in" do
    assert_no_difference 'Post.count' do
      post :create, post: { contend: "Lorem ipsum" }
    end
    assert_redirected_to login_url  
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Post.count' do
      delete :destroy, { id: @post }
    end
    assert_redirected_to login_url
  end

  
end
