require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase

  def setup
    @base_title = "Blissgon"
  end

  test "should get help" do
    get :help
    #ojo solo para pasar el user name de login session#new a signup user#new
    assert_response :success
    assert_select "title", "#{@base_title} |  Help"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "#{@base_title} |  About"
  end

end
