require 'test_helper'

class CommentsControllerTest < ActionController::TestCase

  def  setup
    #@user = users(:michael)
    #@post = @user.posts.create(content: "post comment!")
    #@comment = @user.comments.build(content: "first")
    #@comment = @post.comments.create!(content: "firt comment",user_id: @user.id);
    @comment = comments(:one)
  end

  test "should redirect create when not log in" do
    assert_no_difference 'Comment.count' do
      post :create, :post_id => @comment.post.id , comment: { content: "lorem ipsum" }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not log in" do
    assert_no_difference 'Comment.count' do
      delete :destroy, :id => @comment.id, :post_id => @comment.post.id
    end
  end

end
